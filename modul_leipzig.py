import numpy as _np
import os as _os
import ipywidgets as _ipywidgets
import matplotlib.pyplot as _plt
from joblib import Parallel as _Parallel, delayed as _delayed
import io as _io
from collections import namedtuple as _namedtuple
from IPython.display import display as _display
from matplotlib import colors as _mplcolors
import mdciao as _mdc
import nglview as _ngl
import mdtraj as _md
import msmtools as _msmtools
import pyemma as _pyemma
import mymm as _mymm
import itertools as _itertools
import myplots as _myplots
import io as _io
from matplotlib import ticker as _mticker
from matplotlib import patches as _patches
from sklearn.mixture import GaussianMixture as _GM
import molpx as _molpx


def n_flips_until(p=.25):
    changed = False
    n = 0
    while not changed:
        n += 1
        dice = _np.random.random(1)
        if dice <= p:
            changed = True
        # print(n,flipped,dice)
    return n
def geometric_tests():
    myfig, myax = _plt.subplots(1, 2, figsize=(10, 5))
    for ii in [2, 3, 4, 5, 6]:
        n = [n_flips_until(p=1 / ii) for __ in range(int(1e4))]
        bc = _np.bincount(n)
        x = _np.arange(len(bc))
        bc = bc / bc.sum()
        myax[0].plot(x[1:], bc[1:], "-o", label="%u %3.2f [%6.3f, %6.3f]" % (ii, 1 / ii, _np.mean(n), _np.median(n)))
        x = _np.arange(1, len(n))[::20]
        imean = _np.array([_np.mean(n[1:ii]) for ii in x])
        #istd = _np.mean([_np.std(n[1:ii]) for ii in x[1:]])
        myax[1].plot(x, imean)
        #_plt.fill_between(x, imean - istd, y2=imean + istd, alpha=.25)
    _plt.sca(myax[0])
    _plt.legend()
    _plt.xlabel("number of coinflips")
    _plt.ylabel("PDF")
    _plt.xlim([0, 20])

def geometric_distros():
    _plt.figure(figsize=(20, 10))
    #rng = _np.random.default_rng()

    def gdist(x, p):
        return ((1 - p) ** (x - 1)) * p

    x = _np.arange(1, 100)
    for ii in [2, 3, 4, 5, 6]:
        s = gdist(x, 1 / ii)
        imean = _np.average(x, weights=s)
        ivar = _np.average((imean - x) ** 2, weights=s)
        _plt.plot(x, s / s.max(), "-o", label="p = %4.2f, [x]= %4.2f, var = %4.2f" % (1 / ii, imean, ivar), zorder=-10,
                 alpha=.25)
        # plt.plot(np.bincount(s), "-o", label="$\lambda$ = %4.2f"%l(ii), zorder=-10, alpha=.25)
    n = [n_flips_until(p=1 / 6) for __ in range(int(1e5))]
    bc = _np.bincount(n)
    nmax = _np.min((_np.max(n), len(x)))
    _plt.plot(x[:nmax - 1], bc[1:nmax] / bc.max(), "-x",
             label="p = %4.2f, [x]= %4.2f, std = %4.2f" % (1 / 6, _np.mean(n), _np.var(n)))
    _plt.xlim([0, 20])
    _plt.legend()

def toss_imbalanced_coin():
    n_exp = _ipywidgets.IntText(value=5,
                                description="Number of experiments", min=1,
                                layout={"width": "33%"},
                                style={"description_width": "max-content"})

    p_coin = _ipywidgets.FloatSlider(
        description="coin balance",
        value=.50,
        min=.0, max=1, step=.05,
        layout={"width": "33%"},
        style={"description-width": "initial"})
    run = _ipywidgets.Button(description="run")

    clear = _ipywidgets.Button(description="clear")

    output = _ipywidgets.Output(
        layout={
            "width": "50%",
            'border': '1px solid black'})

    last_run = _ipywidgets.Text(
        style={"description-width": "initial"},
        layout={"width": "50%"}, )

    """
    _plt.figure(figsize=(3,3))
    ifig = _plt.gcf()
    b = _io.BytesIO()
    ifig.savefig(b, format="png", bbox_inches="tight")
    width_px, height_px = [_np.round(ii / 1.25) for ii in ifig.get_size_inches() * ifig.get_dpi()]
    img = _ipywidgets.Image(value=b.getvalue(),
                                     width=width_px,
                                     height=height_px
                                    )
    """
    args = {"n_exp": n_exp,
            "p": p_coin,
            "last_run": last_run,
            "running_n":[],
           # "img": img
            }

    def run_n_experiments(args):
        n=[]
        p = args["p"].value
        flips = 1/p
        sigma = _np.sqrt((1-p)/(p**2))
        with output:
            for ii in range(args["n_exp"].value):
                n.append(n_flips_until(p))
                print("Exp: %4u needed %4u flip(s) until coin changed side"%(ii+args["n_exp"].value*len(args["running_n"]), n[-1]))
            print("----------------------------")
            print("Avg. Flips Needed    %4.2f (σ = %4.2f)"%(_np.mean(n), _np.std(n)))
            print("Avg. Flips Expected  %4.2f (σ = %4.2f)"%(1/p, sigma))
            print()
        args["last_run"].value = 'last run %4.2f ± %4.2f [%4.2f ±% 4.2f] (average±sigma [true ± σ ])' % \
                                 (_np.mean(n),
                                  _np.std(n),
                                  flips,
                                  sigma)
        args["running_n"].append(n)
        """
        cc = 0
        _plt.figure()
        for rn in args["running_n"]:
            for irn in rn:
                _plt.plot(_np.arange(irn), [cc]*(irn),"or")
                _plt.plot(irn, cc,"og")
                cc +=1
        iax = _plt.gca()
        ifig = _plt.gcf()

        iax.yaxis.set_major_formatter(_mticker.FuncFormatter(update_ticks))
        iax.xaxis.set_major_formatter(_mticker.NullFormatter())
        iax.xaxis.set_minor_formatter(_mticker.NullFormatter())


        iax.set_ylim(iax.get_ylim()[::-1])
        style = "Simple, tail_width=0.5, head_width=4, head_length=5"
        kw = dict(arrowstyle=style, color="k", zorder=-10, alpha=.25)
        for ii, i_n in enumerate(n):
            for jj in range(i_n):
                a3 = _patches.FancyArrowPatch((jj, ii), (1+jj, ii),
                                         connectionstyle="arc3,rad=.25",**kw)
                iax.add_artist(a3, )

        iax.set_xticks([])
        b = _io.BytesIO()
        ifig.savefig(b, format="png", dpi=150, bbox_inches="tight")
        args["img"].value = b.getvalue()
        """

    #run_n_experiments(4)

    run_lambda = lambda : run_n_experiments(args)
    clear_lambda = lambda : (setattr(last_run, "value", ""), output.clear_output())
    run.on_click(lambda _ : run_lambda())
    clear.on_click(lambda _ : clear_lambda())
    first_line = _ipywidgets.HBox([n_exp, p_coin, run,clear])
    #second_line = output
    #second_line = _ipywidgets.HBox([output, args["img"]],layout={"width":"99%"})
    second_line = _ipywidgets.HBox([output, last_run],layout={"width":"99%"})
    return _ipywidgets.VBox([first_line, second_line])


def update_ticks(x, pos):
    if _np.mod(x, 1) == 0:
        return "Exp. %u" % x
    else:
        return None

def find_k_using_cointoss():
    n_exp = _ipywidgets.IntText(value=5,
        description="Number of experiments", min=1,
                               layout={"width":"33%"},
                              style={"description_width": "max-content"})
    p_coin = _ipywidgets.FloatSlider(
        description="k",
        value=.50,
        min=.0, max=1, step=.05,
                                    layout={"width":"33%"},
                                    style={"description-width":"initial"})
    shuffle_p = _ipywidgets.Button(description="randomize k")
    toggle_view = _ipywidgets.ToggleButton(description="show k")
    run = _ipywidgets.Button(description="run")
    clear = _ipywidgets.Button(description="clear")
    output = _ipywidgets.Output(
        layout={
            "width" : "50%",
            'border': '1px solid black'})


    args={"n_exp":n_exp,
          "p":p_coin,
          "toggle_show":toggle_view,
          }

    def randomize(args):
        v = _np.random.permutation(_np.linspace(0,1,21))[0]
        args["toggle_show"].value=False
        args["p"].value=v

    def run_n_experiments(args):
        n = []
        with output:
            for ii in range(args["n_exp"].value):
                n.append(n_flips_until(args["p"].value))
            #    print("Exp: %u needed %u flip(s) until coin changed side" % (ii, n[-1]))
            print("Est. tau =   %04.2f steps     (σ  =%5.2f, N=%u)" % (_np.mean(n), _np.std(n), args["n_exp"].value))
            print("Est. k   =   %04.2f steps⁻¹   (σ* =%5.2f, N=%u)" % (1 /_np.mean(n), (1 / _np.std(n))/_np.mean(n), args["n_exp"].value))

            print("------------------------------------")

            print()
        # https://en.wikipedia.org/wiki/Propagation_of_uncertainty#Example_formulae

        # run_n_experiments(4)

    run_lambda = lambda: run_n_experiments(args)
    randomize_lambda = lambda : randomize(args)
    def toggle_lambda(traits):
        owner = traits["owner"]
        if owner.value:
            owner.description = "k = %4.2f (click to hide)"%(args["p"].value)
        else:
            owner.description = "show k"
    clear_output_lambda = lambda: output.clear_output()

    run.on_click(lambda _: run_lambda())
    clear.on_click(lambda _: clear_output_lambda())
    clear.on_click(lambda _ : setattr(toggle_view,"value",False))
    shuffle_p.on_click(lambda _ : randomize_lambda())
    shuffle_p.on_click(lambda _ : clear_output_lambda())
    toggle_view.observe(lambda change : toggle_lambda(change))
    first_line = _ipywidgets.HBox([n_exp, shuffle_p, run, clear, toggle_view])
    second_line = _ipywidgets.HBox([output], layout={"width": "99%"})
    return _ipywidgets.VBox([first_line, second_line])

def convergence():
    _plt.ioff()
    tau_selector=_ipywidgets.Text(description="tau values (csv)",
                                 value="5, 10, 20, 25",
                                 layout={"width":"75%"},
                                 style={"description_width":"initial"},
                                 continuous_update=False)
    run = _ipywidgets.Button(description="run")
    rescale_x = _ipywidgets.Checkbox(description="x-axis: rescale sample size (steps) with tau",
                                    style={"description_width": "initial"})
    rescale_y = _ipywidgets.Checkbox(description="y-axis: rescale estimated value with tau",
                                    style={"description_width": "initial"})
    n_steps = _ipywidgets.IntSlider(description="number of steps used",
                                   value=2500,
                                   min=10,
                                   step=500,
                                   max=1e5,
                                   style={"description_width": "initial"},
                                   continuous_update=False,
                                   )
    xlim = _ipywidgets.FloatSlider(min=0,
                                  value=n_steps.value,
                                   step=50,
                                  description="x-axis: limit",
                                  style={"description_width": "initial"},
                                  continuous_update=False,
                                  max=n_steps.value)
    img_wdg = _ipywidgets.Image()
    img_wdg.width="1px"
    img_wdg.height="1px"

    args = {"n_steps": n_steps,
            "taus": tau_selector,
            "rescale_x": rescale_x,
            "rescale_y": rescale_y,
            "image":img_wdg,
            "xlim":xlim,
            "n_runs":0
            }
    def run_exps_lambda():
        #print("running!",args["n_runs"])
        args["n_runs"]+=1
        args["results"] = run_experiments(args["taus"].value, args["n_steps"].value,parallel=False)

    def plot_lambda(owner):
        xlim = [0, args["xlim"].value]
        try:
            if owner["owner"] is args["n_steps"]:
                args["xlim"].max=args["n_steps"].value
                xlim = [0, args["n_steps"].value]
        except:
            pass
        _plt.ioff()
        from matplotlib import get_backend as _get_backend, use as _mpluse
        backend_ = _get_backend()
        _mpluse("Agg")  # Prevent showing stuff
        ifig = plot_tau_convergence(args["results"],
                             args["rescale_x"].value,
                             args["rescale_y"].value,
                                   xlim=xlim,
                                    ylim=[0,2*_np.max([_np.float(ff) for ff in args["taus"].value.split(",") if len(ff)>0])],
                                    )
        _mpluse(backend_)
        fig2pngwdg(ifig,args["image"])
    def run_lambda(change):
        run_exps_lambda()
        plot_lambda(change)

    run.on_click(lambda _ : run_lambda(None))
    tau_selector.observe(lambda _ : run_lambda(None),names="value")
    rescale_x.observe(lambda change : plot_lambda(change),names="value")
    rescale_y.observe(lambda change : plot_lambda(change),names="value")
    n_steps.observe(lambda change : run_lambda(change),names="value")
    xlim.observe(lambda change : plot_lambda(change), names="value")

    return _ipywidgets.VBox([tau_selector,n_steps,
                            xlim,
                            rescale_x, rescale_y, run, img_wdg],
                                            layout={"width":"100%"})
def consume_n_flips(n, p):
    flips_consumed=0
    out=[]
    while flips_consumed<n:
        out.append(n_flips_until(p))
        flips_consumed+=out[-1]
    return out

def run_experiments(taus, max_flips, parallel=True):
    if isinstance(taus, str):
        taus = [float(ii) for ii in taus.split(",") if len(ii) > 0]

    run_n_exps = lambda n, p: consume_n_flips(n, p)
    if parallel:
        experiments = _Parallel(n_jobs=len(taus))(_delayed(run_n_exps)(max_flips, 1 / tau) for tau in taus)
    else:
        experiments = [run_n_exps(max_flips, 1/tau) for tau in taus]
    experiments = {tau: ni for tau, ni in zip(taus, experiments)}
    return experiments

def plot_tau_convergence(experiments,
                         rescale_x=False,
                         rescale_y=False,
                         xlim=None,
                         ylim=None
                        ):

    _plt.ioff()
    fig = _plt.figure(figsize=(15, 5))
    for tau, ni in experiments.items():
        summed_samples_up_to_nth_experiment = _np.cumsum(ni)
        n_experiments_up_to_nth_experiment = _np.arange(1, len(ni) + 1)
        running_av = summed_samples_up_to_nth_experiment / n_experiments_up_to_nth_experiment

        if rescale_x:
            summed_samples_up_to_nth_experiment=summed_samples_up_to_nth_experiment/tau

        if rescale_y:
            running_av=running_av/tau

        line = _plt.plot(summed_samples_up_to_nth_experiment, running_av, label="true $\\tau_0$ = %u" % tau)[0]
        #stds = [_np.std(n[:ii]) for ii in range(1, len(ni) + 1)]
        if rescale_y:
            _plt.axhline(1, color=line.get_color(), ls="--", alpha=.5)
        else:
            _plt.axhline(tau, color=line.get_color(), ls="--", alpha=.5)

        # _plt.fill_between(x, running_av-stds, running_av+stds,alpha=.25)
    _plt.legend(ncol=2)
    _plt.xlabel("sample size / %s" % {False: "steps",
                                     True: "$\\tau_0$"}[rescale_x])
    _plt.ylabel("estimated $\\tau_{\mathrm{obs}}$ / %s" % {False: "steps",
                                           True: "$\\tau_0$"}[rescale_y])
    _plt.xlim([0, summed_samples_up_to_nth_experiment[-1]])
    if rescale_x:
        _plt.xlim([0,250])
    if rescale_y:
        _plt.ylim([0,2])
    if xlim is not None and not rescale_x:
        _plt.xlim(xlim)
    if ylim is not None and not rescale_y:
        _plt.ylim(ylim)
    return fig

def plot_exponential_tau_distribution(ks=[.2, .5, .9], n_exps=1e4):
    for p in ks:
        n = [n_flips_until(p) for ii in range(int(n_exps))]
        x, y = _np.arange(1, _np.max(n) + 1), _np.bincount(n)[1:]
        line = _plt.plot(x, y / y.max(), "-o", label="$k = %2.1f,\\tau$ = %2.1f" % (p, 1 / p))[0]
        _plt.axvline(_np.mean(n), color=line.get_color(), ls="--")
    _plt.legend()
    _plt.ylabel("freq")
    _plt.xlabel("$\\tau$")
    _plt.xlim([0, 20])
    _plt.xticks(_np.arange(0, 20)[::2])

def fig2pngwdg(ifig, iwdg=None):
    b = _io.BytesIO()
    width_px, height_px = [_np.round(ii / 1.25) for ii in ifig.get_size_inches() * ifig.get_dpi()]
    ifig.savefig(b, format="png", bbox_inches="tight")
    if iwdg is None:
        iwdg = _ipywidgets.Image(value=b.getvalue(),
                                 width=width_px,
                                 height=height_px)
    else:
        iwdg.value = b.getvalue()
        iwdg.width = width_px
        iwdg.height = height_px
    b.close()
    _plt.close(ifig)
    return iwdg

def evaluate_Gaussians(x, Gs):
    eval_G = lambda x, G: G.N * _np.exp(-(x - G.xo) ** 2 / G.k)

    return _np.vstack([eval_G(x, iG) for iG in Gs]).sum(0)

def assign_Gaussians(x, Gs):
    eval_G = lambda x, G: G.N * _np.exp(-(x - G.xo) ** 2 / G.k)
    return _np.vstack([eval_G(x, iG) for iG in Gs]).argmax(0)

def screen_Gaussians(n=2,figsize=(15,8), init_Gaussians=None):
    _plt.figure(figsize=figsize)
    iax = _plt.gca()
    x = _np.linspace(-1.5, 1.5, 200)
    Gs = {}
    if init_Gaussians is None:
        Gs["val"] = initialize_N_Gaussians(n)
    else:
        Gs["val"] = init_Gaussians
    def V(x,Gs,q=False):
        _V = -evaluate_Gaussians(x, Gs)
        if q:
            _V = quadratic_extrema(_V)
        return _V

    #V = lambda x,Gs: evaluate_Gaussians(x, Gs)
    iwdg = fig2pngwdg(iax.figure)
    iwdg.layout.width="87%"
    colors = _plt.rcParams['axes.prop_cycle'].by_key()["color"]
    colors= [_mplcolors.to_hex(_mplcolors.to_rgba(colors[idx], .35),keep_alpha=True) for idx in range(len(colors))]

    n_counter = _ipywidgets.BoundedIntText(continuous_update=False,
                                           description="number of Gaussians",
                                           layout={"width":"80%"},
                                           style={"description_width": "initial"},
                                           value=n,
                                           min=1)
    picker = _ipywidgets.Dropdown(options=[ii + 1 for ii in range(n)],
                                  #description="active Gaussian",
                                  layout={"width":"initial"},
                                  style={"description_width": "initial"},
                                  )

    xo_slider = _ipywidgets.FloatSlider(min=-1.5, max=1.5,
                                        value=0, description="position",
                                        continuous_update=False,
                                        layout={"width":"99%"}
                                        )
    N_slider = _ipywidgets.FloatSlider(min=0, max=10,
                                       value=0, description="depth",
                                       continuous_update=False,
                                       layout={"width": "99%"}
                                       )
    k_slider = _ipywidgets.FloatSlider(min=0.001, max=2.5, value=1,
                                       description="width",
                                       step=0.005,
                                       continuous_update=False,
                                       layout={"width": "99%"}
                                       )


    indicator = _ipywidgets.Button(description="change Gaussian nr.:",
                                   style={"description_width": "initial"}
                                   )

    boltz = _ipywidgets.ToggleButton(description="show Boltzmann distribution",
                                     style={"description_width":"max-content"},
                                     layout={"width":"50%"}
                                     )
    indv_G = _ipywidgets.ToggleButton(description="hide Gaussians",
                                     style={"description_width":"max-content"},
                                     layout={"width":"19%"}
                                     )

    taubox = _ipywidgets.Text(description="slowest timescale",
                              style={"description_width": "initial"}
                                  )

    active_box = _ipywidgets.VBox(
        [_ipywidgets.HBox([indicator, picker, boltz,
                           #taubox
                           ],
                          layout={"width": "100%"}),
         xo_slider, N_slider,
         k_slider],
         layout={"width":"99%"}
    )
    myVBOX = _ipywidgets.VBox([iwdg,_ipywidgets.HBox((n_counter,indv_G), layout={"width": "100%"}),
                               active_box],
                              # layout={"width": "150px"},
                              layout={"width": "99%",
                                      "align_content":"center",
                                      "align_items": "center"}

                              )

    def replot(Gs, n_counter, iwdg):
        iax.clear()
        Gs["val"] = initialize_N_Gaussians(n_counter.value, old_Gs=Gs["val"])
        iV = V(x,Gs["val"],q=False)
        iax.plot(x, iV,color="gray",label="total Energy")
        filled_gaussians = [iax.fill_between(x, V(x, [iG],q=False),color=colors[ii],label="%u"%(ii+1)) for ii, iG in enumerate(Gs["val"])]
        iax.set_xlim(x[[0, -1]])
        iax.set_ylabel("Energy 'V' / arb. u.")
        iax.set_xlabel("Reaction Coordinate")
        iax.set_xlim(x[[0,-1]])
        if boltz.value:
            b = _np.exp(-iV) / _np.sum(_np.exp(-iV))
            b -= b.min()
            b /= b.max()
            scaled_yspan = _np.diff(iax.get_ylim())*.25
            baseline = iax.get_ylim()[0]-scaled_yspan
            b +=baseline
            iax.fill_between(x,baseline,b,
                             #linecolor=None,
                             alpha=.25,color="gray",
                             label="$\\rho \\propto e^{-V}$")
        if indv_G.value:
            for PC in filled_gaussians:
                PC.remove()

        iax.legend(loc="best")
        fig2pngwdg(iax.figure, iwdg)
        myVBOX.Gaussians=Gs["val"]
        """
        tx, tV = x_and_V_from_Gaussians(Gs["val"],
                                                    pad=0,
                                                    quadratic_at_the_boundary=False,
                                                    strip_borders=True,
                                                    resolution=200,
                                                    # eps=1e5,
                                                    stride=1)
        T = _mymm.construct_T_with_V(tV, tx)
        T = _msmtools.generation.transition_matrix_metropolis_1d(tV)
        MSM =_pyemma.msm.MSM(T)

        t = MSM.timescales()[0]
        print("t mine",t)
        taubox.value='~ %u (~1e^%u steps)'%(t,_np.round(_np.log10(t)))
        """

    def picker_update(change):
        picker.options=[ii+1 for  ii in range(change.new)]

    def update_affected_G(change):
        xo_slider.value = Gs["val"][change.new-1].xo
        N_slider.value = Gs["val"][change.new-1].N
        k_slider.value = Gs["val"][change.new-1].k

    def update_value(change,attribute):
        idx = picker.value-1
        iG = Gs["val"][idx]
        if attribute=="xo":
            iG = Gaussian(iG.N, change.new, iG.k)
        elif attribute=="N":
            iG = Gaussian(change.new, iG.xo, iG.k)
        elif attribute=="k":
            iG = Gaussian(iG.N, iG.xo, change.new)
        Gs["val"][idx]=iG
        indicator.style.button_color=colors[idx]
        replot(Gs,n_counter,iwdg)

    replot(Gs,n_counter,iwdg)
    indicator.style.button_color = colors[0]
    xo_slider.value = Gs["val"][0].xo
    N_slider.value = Gs["val"][0].N
    k_slider.value = Gs["val"][0].k

    lambda_replot = lambda __: replot(Gs, n_counter, iwdg)
    lambda_picker_update = lambda change : picker_update(change)
    lambda_update_affected_G = lambda change : update_affected_G(change)
    lambda_update_value = lambda change,attribute : update_value(change,attribute)
    #lambda_update_boltzman = lambda __ : replot(Gs,n_counter,iwdg)

    n_counter.observe(lambda __: lambda_replot(None), names="value")
    n_counter.observe(lambda change : lambda_picker_update(change),names="value")
    picker.observe(lambda change : lambda_update_affected_G(change), names="value")
    xo_slider.observe(lambda change : lambda_update_value(change,"xo"),names="value")
    N_slider.observe(lambda change : lambda_update_value(change, "N"),names="value")
    k_slider.observe(lambda change : lambda_update_value(change, "k"),names="value")
    boltz.observe(lambda _ : lambda_replot(None),names="value")
    indv_G.observe(lambda _ : lambda_replot(None), names="value")

    return myVBOX

Gaussian = _namedtuple("Gaussian", ["N", "xo", "k"])

def initialize_N_Gaussians(n,old_Gs=None):

    Gs = []
    for ii, (N, xo, k) in enumerate(_np.random.random((n,3))):
        try:
            Gs.append(old_Gs[ii])
        except:
            #print(N,xo,k)
            #3xo*=2
            xo-=.5
            xo*=2
            k/=10
            N*=5
            Gs.append(Gaussian(N,xo,k))
    return Gs

def x_and_V_from_Gaussians(Gs, eps=1e-3, plot=False, pad=0,
                           quadratic_at_the_boundary=True,
                           resolution=200,
                           strip_borders=True,
                           stride=50):
    xos = [iG.xo for iG in Gs]
    left, right = _np.min(xos), _np.max(xos)
    width = 5*_np.max([iG.k for iG in Gs])
    N = _np.max([iG.N for iG in Gs])
    x = _np.linspace(left-N*width,right+N*width,resolution)
    V = -evaluate_Gaussians(x, Gs)
    if not strip_borders:
        return x[::stride],V[::stride]
    first = _np.flatnonzero(_np.abs(V)>eps)[0]
    last = _np.flatnonzero(_np.abs(V)[::-1]>eps)[0]
    if last==0:
        last=1
    span = _np.diff(x[[first, -last]])
    xfirst, xlast = x[first] - span * pad, x[-last] + span * pad
    x = _np.linspace(xfirst, xlast, resolution).squeeze()
    new_V  = -evaluate_Gaussians(x, Gs)
    if quadratic_at_the_boundary:
        new_V = quadratic_extrema(new_V)

    if plot:
        _plt.plot(x,new_V)

    return x[::stride], new_V[::stride]


def resmap2atommap(topA2topb, topA,topB):
    #TODO I have done this before, cannot find it grrrr
    r"""
    Take residue-to-residue map and expand it to an atom-to-atom-map using atom-names
    Parameters
    ----------
    topA2topb : dict
    topA : mdtraj.top
    topB : mdtraj.top

    Returns
    -------
    a2a : dict

    """

    out_dict = {}
    for idxA, idxB in topA2topb.items():
        resA = topA.residue(idxA)
        resB = topB.residue(idxB)

        atoms_A = {aa.index:aa.name for aa in resA.atoms}
        atoms_B = {aa.index:aa.name for aa in resB.atoms}
        for idx, name in atoms_A.items():
            idxB = {ii:name for ii, iname in atoms_B.items() if name==iname}
            #print(idxB)
            assert(len(idxB)==1)
            key = list(idxB.keys())[0]
            out_dict[idx]=key
            atoms_B.pop(key)


    return out_dict

def show_activation():

    geom_3CAP, geom_1U19 = get_3CAP_1U19()
    liig = get_liig(geom_1U19, geom_3CAP)
    
    iwd = _ngl.show_mdtraj(liig)
    iwd.remove_cartoon(component=0)
    iwd.add_cartoon(color="gray",component=0,opacity=".5")
    iwd.add_trajectory(geom_1U19, 
                              gui=True
                             )
    iwd.remove_cartoon(component=1)
    iwd.add_cartoon(color="red",component=1)
    iwd.add_trajectory(geom_3CAP)
    iwd.remove_cartoon(component=2)
    iwd.add_cartoon(color="green",component=2)
    return iwd

def get_3CAP_1U19():
    geom_3CAP = _mdc.nomenclature.PDB_finder("3CAP")[0]
    if not _os.path.exists("3CAP.pdb"):
        geom_3CAP.save("3CAP.pdb")
    geom_1U19 = _mdc.nomenclature.PDB_finder("1U19")[0]
    if not _os.path.exists("1U19.pdb"):
        geom_1U19.save("1U19.pdb")
    ops_atoms_3CAP = _mdc.fragments.get_fragments(geom_3CAP.top, verbose=False, atoms=True)[0]
    geom_3CAP = geom_3CAP.atom_slice(ops_atoms_3CAP)

    ops_atoms_1U19 = _mdc.fragments.get_fragments(geom_1U19.top, verbose=False, atoms=True)[0]
    geom_1U19 = geom_1U19.atom_slice(ops_atoms_1U19)

    return geom_3CAP, geom_1U19

def get_liig(reactants,products,n_steps=20):
    map1, map2 = _mdc.utils.sequence.maptops(products.top, reactants.top)
    map1ats = resmap2atommap(map1, products.top, reactants.top);

    products.superpose(reactants,
                        atom_indices=list(map1ats.keys()),
                        ref_atom_indices=list(map1ats.values()))

    xyz_r, xyz_p = reactants.xyz[0, list(map1ats.values())], products.xyz[0, list(map1ats.keys())]
    dxyz = (xyz_p - xyz_r) / (n_steps + 1)
    liig = _md.Trajectory([xyz_r + ii * dxyz for ii in range(n_steps)],
                          topology=products.topology.subset(list(map1ats.keys())))

    return liig
def MD(x, V, traj_length=1e5, start=None, n_traj=1, n_smooth=10, stride=1,
       panelheight=5,
       Gaussians=None):

    colors = _plt.rcParams['axes.prop_cycle'].by_key()["color"]

    T = _pyemma.msm.MSM(_msmtools.generation.transition_matrix_metropolis_1d(V))
    nstep = 1
    sampler = _msmtools.generation.api.MarkovChainSampler(T.transition_matrix,
                                                         nstep,
                                                         )
    traj_length = int(traj_length)
    if start is not None:
        start = _np.argmin(_np.abs(x - start))


    trajs = sampler.trajectories(n_traj, traj_length,
                                 start = start
                                 )

    myfig, myax = _plt.subplots(n_traj + 1, 2,
                                sharex="col",
                                sharey=True,
                                squeeze=False,
                                gridspec_kw={'width_ratios': [10, 1]},
                                figsize=(2 * panelheight, n_traj + 1 * panelheight))
    t = _np.arange(traj_length)
    t2 = _mdc.utils.lists.window_average_fast(t, n_smooth)
    pi, x_pi = _np.histogram(x[_np.hstack(trajs)],
                             bins=50)
    h = _np.diff(x_pi)[0]*2
    myax[-1,1].barh(x_pi[:-1],
                   width=pi/pi.max(),
                   #alpha=.25,
                   #height=_np.diff(x_pi)[0],
                   height=h,
                   #alpha=.25,
                   edgecolor=None)

    rho = _np.exp(-V)
    rho /= rho.max()
    myax[-1,1].plot(rho,x,color="r",zorder=10,lw=2)
    #myax[0,1].set_ylim(x[[0,-1]])
    #myax[0, 0].set_ylim(x[[0, -1]])
    myax[-1,0].set_ylabel("x(t)")
    myax[-1,0].set_title("all trajs\n%u x %u = %u steps in total"%(n_traj, traj_length, n_traj*traj_length))
    myax[-1, 1].set_title("$\\rho$")
    if Gaussians is not None:
        g_voronoi = _mdc.utils.lists.contiguous_ranges(assign_Gaussians(x, Gaussians))
        #assert all([len(val)==1 for val in g_voronoi.values()])
        g_voronoi = {key : val[0] for key,val in g_voronoi.items()}
    for tt, (itraj, (iax,jax)) in enumerate(zip(trajs,myax[:-1])):
        y = x[itraj]
        y2 = _mdc.utils.lists.window_average_fast(y,n_smooth)

        myax[-1,0].plot(t2[::stride],y2[::stride],color='gray', alpha=.25)
        iax.plot(t2[::stride],y2[::stride])

     #   iax.set_ylim(x[[0, -1]])
     #   jax.set_ylim(x[[0, -1]])
        pi, x_pi = _np.histogram(x[itraj],
                                 bins=50)
        jax.barh(x_pi[:-1],
              width=pi / pi.max(),
              # alpha=.25,
              # height=_np.diff(x_pi)[0],
              height=h,
              # alpha=.25,
              edgecolor=None)
        jax.set_title("$\\rho_%u$"%(tt+1))
        iax.set_ylabel("x(t)")
        iax.set_title("traj %u"%(tt+1))
        for key, lims in g_voronoi.items():
            iax.axhspan(x[lims[0]], x[lims[-1]], color = colors[key], edgecolor=None, alpha=.25)
    iax.set_ylim(*[x[[-1,0]]])
    iax.set_xlim([0, t2[::stride].max()])
    iax.set_xlabel("t / steps")
    xticks = _plt.xticks()[0]
    myfig.tight_layout()
    return trajs

from scipy import optimize as _op
def fit_x2(f, return_fit=False):
    N = _np.abs(min(f))
    xo= len(f)/2
    k=500
    my_x2 = lambda x, N,xo,k : ((x-xo)**2)/k-N
    p0 = [N,xo,k]
    ii,jj = int(_np.round(len(f)*.4)), int(_np.round(len(f)*.6))
    popt, pcov = _op.curve_fit(my_x2, _np.arange(0, len(f))[ii:jj], f[ii:jj], p0=p0)
    fit = my_x2(_np.arange(0, len(f)), popt[0], popt[1], popt[2])
    if return_fit:
        return popt, pcov, fit
    else:
        return popt, pcov

def quadratic_extrema(V):
    left_idx = _np.flatnonzero(_np.diff(V)>0)[0]
    fake_V = _np.hstack((V[:left_idx],V[:left_idx][::-1]))
    popt, _, fit = fit_x2(fake_V, return_fit=True)
    _V = V.copy()
    _V[:left_idx]=_np.average(_np.vstack((_V[:left_idx],fit[:left_idx])),axis=0)
    right_idx = _np.flatnonzero(_np.diff(V[::-1])>0)[0]
    fake_V = _np.hstack((V[-right_idx:][::-1],V[-right_idx:]))
    popt, _, fit = fit_x2(fake_V, return_fit=True)
    #_V[:left_idx] = _np.average(_np.vstack((_V[:left_idx], fit[:left_idx])), axis=0)
    _V[-right_idx:]=_np.average(_np.vstack((_V[-right_idx:],fit[-right_idx:])),axis=0)
    return _V


def sample_Ntimes_w_one_over_mu_powerK(MSM, N=1, K=1, x=None, extra_p=None):
    one_over_mu = (1. / MSM.stationary_distribution) ** K
    p = one_over_mu
    if extra_p is not None:
        p *= p
    sample = _np.random.choice(MSM.active_set, size=N, p= p/p.sum())
    if x is not None:
        sample = x[sample]

    return sample


def plot_sample_freq_on_V(sample, V, x, ax=None):
    samples_unique = _np.unique(sample)
    samples_freq = _np.bincount(sample)[samples_unique]
    samples_and_freqs = _np.vstack((samples_unique, samples_freq))

    if ax is None:
        __, ax = _plt.subplots(1, 1, figsize=(10, 5))

    for (pos, rad) in samples_and_freqs.T:
        ax.plot(x[pos], V[pos], ' or', markersize=_np.sqrt(rad) * 5)

    ax.fill_between(x, _np.zeros_like(x), V, color='gray', alpha=.75)


def adaptive_sampler(n_epochs, n_traj_per_epoch, nt_per_traj,
                     tau=1, start_x=None, adapt=True, sampler=None, V=None, x=None):
    print("total number of steps %u x %u x %u = %u (epochs x trajs per epoch x traj lenght" % (
    n_epochs, n_traj_per_epoch, nt_per_traj, n_epochs * n_traj_per_epoch * nt_per_traj))
    if sampler is None:
        T = _pyemma.msm.MSM(_msmtools.generation.transition_matrix_metropolis_1d(V))
        nstep = 1
    sampler = _msmtools.generation.api.MarkovChainSampler(T.transition_matrix,
                                                          nstep,
                                                          )
    MSMs = []
    samples = []
    batch_trajs = []
    start=None
    if start_x is not None:
        start = _np.argsort(_np.abs(start_x-x))[0]

    # Initialize taus
    current_trajs = sampler.trajectories(n_traj_per_epoch, nt_per_traj, start=start)
    print("epoch ntrajs    si  sv    %space visited")
    print("===== ======    ==  ==    ===============")
    extra_p = -((_np.abs(x - x.mean())) ** 3)
    extra_p -= extra_p.min()
    extra_p /= extra_p.max()
    #_plt.plot(x, extra_p/extra_p.max())
    #_plt.plot(x, V/V.max())
    for ii in _np.arange(n_epochs):

        # Compute the MSM with the current batch of trajs
        iMSM = _pyemma.msm.estimate_markov_model(current_trajs, tau)

        # Sample from it for the next gen
        if adapt:
            isamples = sample_Ntimes_w_one_over_mu_powerK(iMSM, N=n_traj_per_epoch, K=1, extra_p=extra_p[iMSM.active_set])
            # Generate the new trajs
            new_trajs = [sampler.trajectory(nt_per_traj, start=i_start) for i_start in isamples]
        else:
            new_trajs = sampler.trajectories(n_traj_per_epoch, nt_per_traj)
            isamples = _np.hstack(itraj[0] for itraj in new_trajs)
        # Update the current_batch
        current_trajs = current_trajs+new_trajs

        # Inform
        print('  %u      %u    %u %u       %4.2f'%(ii, len(current_trajs), iMSM.nstates, iMSM.nstates_full,_np.round(iMSM.nstates/T.nstates*100)))

        # Store the separated quantities
        MSMs.append(iMSM)
        samples.append(isamples)

    return MSMs, samples


def plot_adaptive(T, MSMs, samples, V,x, timescales=None):
    if timescales is not None:
        print("True  timescales :", (['%07.1f' % l for l in timescales]))
        print("========================================================")

    for (iMSM, isample) in zip(MSMs, samples):
        __, myax = _plt.subplots(1, 2, figsize=(10, 5), sharey=True)
        _myplots.show_LEVS_from_T(T.transition_matrix,
                                  #iMSM.transition_matrix,
                                  cpi=_np.bincount(_np.hstack(iMSM.discrete_trajectories_full), minlength=len(x)),
                                  axes_list=myax[0],
                                 connected_set=iMSM.active_set,
                                 statemax=1,
                                 potential=V,
                                 fake_renorm=False,
                                 x_axis=x,
                                 pad=1.,
                                 info_text=False)

        plot_sample_freq_on_V(isample, V / V.max() * myax[0].get_ylim()[1] * .8, x, ax=myax[1])
        itimescales = -1. / _np.log(_np.abs(iMSM.eigenvalues()[1:4]))
        print("Model timescales :", ['%08.4f' % l for l in itimescales])

def show_Ea_curve(try_Hbox=False, panelheight=5):
    pos = _np.array([[7.02547690e+01, 5.50459441e-03],
                     [1.30790480e+02, 4.12845865e-03],
                     [1.91326200e+02, 2.75229721e-03],
                     [2.51861910e+02, 1.37616145e-03],
                     [3.12397630e+02, 0.00000000e+00],
                     [3.27950090e+02, 3.95642563e-03],
                     [3.42799420e+02, 1.10091888e-02],
                     [3.57447850e+02, 3.45757096e-02],
                     [3.72397630e+02, 8.80734079e-02],
                     [3.83100990e+02, 1.56576640e-01],
                     [3.93237380e+02, 2.53591912e-01],
                     [4.02418180e+02, 3.71976373e-01],
                     [4.10254770e+02, 5.04587145e-01],
                     [4.22913810e+02, 6.99789450e-01],
                     [4.37583640e+02, 8.57236736e-01],
                     [4.51446170e+02, 9.62212434e-01],
                     [4.61683340e+02, 1.00000000e+00],
                     [4.77403050e+02, 9.58701157e-01],
                     [4.93257680e+02, 8.64537004e-01],
                     [5.07253710e+02, 7.57829108e-01],
                     [5.17397630e+02, 6.78899062e-01],
                     [5.28361170e+02, 6.02706336e-01],
                     [5.37544780e+02, 5.45078946e-01],
                     [5.47904820e+02, 4.99315721e-01],
                     [5.62397630e+02, 4.58715593e-01],
                     [5.81315000e+02, 4.33680023e-01],
                     [6.03201160e+02, 4.23872548e-01],
                     [6.21292700e+02, 4.22771018e-01],
                     [6.28826200e+02, 4.23853205e-01],
                     [6.73647630e+02, 4.23853205e-01],
                     [7.18469060e+02, 4.23853205e-01],
                     [7.63290480e+02, 4.23853205e-01],
                     [8.08111910e+02, 4.23853205e-01]])
    geom_3CAP, geom_1U19 = get_3CAP_1U19()
    liig = get_liig(geom_1U19, geom_3CAP, len(pos))
    output = _ipywidgets.Output()
    _plt.close("all")
    def create_fig():
        fig = _plt.figure(figsize=(panelheight, panelheight))
        _plt.plot(pos[:, 0], pos[:, 1], "-k")
        _plt.xticks([])
        _plt.yticks([])
        _plt.xlabel("reaction coordinate")
        _plt.ylabel("Energy")
        # plt.gca().set_frame_on(False)
        fig.canvas.toolbar_position = 'bottom'
        return fig
    def create_widget(iax):
        iwd, iax = _molpx.visualize.sample(pos, liig, iax,
                                          plot_path=False,
                                          crosshairs=False,
                                          superpose=False,
                                          clear_lines=False,
                                          dot_color="gray",
                                          )
        iwd.remove_cartoon(component=0)
        iwd.add_cartoon(color="gray", component=0,
                        # opacity=".5"
                        )
        iwd.add_trajectory(geom_1U19,
                           gui=True,
                           )
        iwd.remove_cartoon(component=1)
        iwd.add_cartoon(color="red", component=1,
                        opacity=".10"
                        )
        iwd.add_trajectory(geom_3CAP)
        iwd.remove_cartoon(component=2)
        iwd.add_cartoon(color="green", component=2,
                        opacity=".20"
                        )
        iwd._set_size(*['%fin' % inches for inches in fig.get_size_inches()])
        return iwd, iax
    if try_Hbox:
        with output:
            fig = create_fig()
            iwd, iax = create_widget(_plt.gca())

        return _ipywidgets.HBox([iwd, output])
    else:
        fig = create_fig()
        iwd, iax = create_widget(_plt.gca())
        return iwd
