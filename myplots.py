import numpy as _np
from sys import stdout as _stdout
import pyemma.msm as msm
import logging
from pyemma.coordinates.clustering import RegularSpaceClustering as _RegularSpaceClustering
from pyemma.util.annotators import deprecated
import matplotlib.pyplot as _plt
from matplotlib import rcdefaults
from scipy.spatial.distance import cdist as _cdist
#from mymetrics import myregspace
import matplotlib.axes._subplots as _asp
from matplotlib.cbook import silent_list
#from sklearn.mixture import GMM
from scipy.spatial import cKDTree as _cKDTree
from matplotlib.widgets import AxesWidget as _AxesWidget
from IPython.display import display as ipydisplay
from collections import defaultdict as _defaultdict 

_mycolors=[
         'blue',
         'green',
         'red',
         'cyan',
         'magenta',
         'yellow',
         'lime',
         'maroon',
         'navy',
         'olive',
         'orange',
         'purple',
         'teal',
]

myvmdcolors = [ 
'blue',
 'red',
 'gray',
 'orange',
 'yellow',
 'tan',
 'silver',
 'green',
 'white',
 'pink',
 'cyan',
 'purple',
 'lime',
 'mauve',
 'ochre',
 'iceblue',
    # black
 'yellow2',
 'yellow3',
 'green2',
 'green3',
 'cyan2',
 'cyan3',
 'blue2',
 'blue3',
 'violet',
'violet2',
'magenta'
'magenta2',
'red2',
'red3',
'orange2',
'organge3']

mycmap=[
'Blues',
'Reds',
'Greys',
'Oranges',
'YlOrBr',
'Greens'
]


mycolors = _mycolors+_mycolors+_mycolors #man, am *I* lazy

def force_iterable(a):
    """Make iterable"""
    try:
        a.__iter__
        return a
    except:
        return [a]

def ck_plot(MSMs, K = 5, nsets = 2,
            sets = None, dt = 1.0, 
            tunits = 'frames',
            show_population = False,
            ylim = [0,1],
            panelsize = 5,
            ncols = 2,
            legend = 0, 
            alpha = 0.10,
            fontsize = 20,
            savefig = True):

    #_plt.rc('text', usetex=True)
    _plt.rc('font', family='serif', )
    #_plt.rcParams['text.latex.unicode']=True
    
    
    # Convert to iterable
    if not isinstance(MSMs, list):
        MSMs = [MSMs]
        
    # Compute the maximum lag (in frames) that the MSMs will be propagated to
    max_frames = _np.max([iMSM.lagtime*K for iMSM in MSMs])
    
    # Number of columns of the subplot
    nrows=_np.ceil(nsets*1./ncols).astype('int') 
    
    # Iterate over MSMs    
    for mm, iMSM in enumerate(MSMs):
        K = _np.round(max_frames/iMSM.lagtime)
        p_MSM, p_MD, eps_MD, set_pops = msm.cktest(iMSM, K, nsets = nsets, sets = sets, full_output=True)
        
        # Create subplots
        myfig, myax = _plt.subplots(ncols=ncols, nrows = nrows, 
                                   sharex=True, sharey=True, figsize=(ncols*panelsize, panelsize*nrows))

        # Create time-axis in frames*dt
        time_ax = _np.arange(len(p_MSM))*iMSM.lagtime*dt
        for ii, (i_pMSM, i_pMD, i_epsMD, i_pops) in enumerate(zip(p_MSM.T, p_MD.T, eps_MD.T, set_pops.T)):


            pidx = _np.unravel_index(ii, myax.shape)
            myax[pidx].plot(time_ax, i_pMSM, '-o', color=mycolors[ii], 
                            label='pMSM$_%u$, $\\tau = %4.1f\ %s$'%(ii, iMSM.lagtime*dt, tunits)
                            )
            #myax[pidx].plot(time_ax, i_pMSM, ' o', ms=_np.sqrt(100.*(mm+1)),                         
            #                markerfacecolor=mycolors[ii],
                            #markerfacecolor='none',
            #                markeredgecolor='none', 
            #                zorder=len(myMSMs)-mm)

            myax[pidx].plot(time_ax, i_pMD,'--',
                            label='pMD$_%u \pm \sigma$, $\\tau = %4.1f\ %s$'%(ii, iMSM.lagtime*dt, tunits),
                            color=mycolors[ii],
                            )


            myax[pidx].fill_between(time_ax, i_pMD-i_epsMD, i_pMD+i_epsMD,
                                    alpha=alpha,
                                    color=mycolors[ii],
                                    )
            myax[pidx].plot(time_ax, ' ')
            myax[pidx].plot(time_ax, ' ')

            if show_population:
                pp = _np.round(len(i_pops)/2)
                myax[pidx].text(time_ax[pp], i_pMD[pp]+.05,'%3.1f'%((i_pops[pp])*100.), rotation=45)
                myax[pidx].text(time_ax[-1], i_pMD[-1]+.05,'%3.1f'%((i_pops[-1])*100.), rotation=45)
  
            # Visual aids
            if ylim[1] > 1:
               myax[pidx].plot(time_ax, _np.ones_like(time_ax), '--k')
            if ylim[0] < 0:
               myax[pidx].plot(time_ax, _np.zeros_like(time_ax), '--k')

            #myax[pidx].plot(time_ax, i_pMD, ' o', ms=_np.sqrt(100.*(mm+1)),                         
            #                markerfacecolor=mycolors[ii],
            #                alpha=.25, 
            #                #markerfacecolor='none',
            #                markeredgecolor='none', 
            #                zorder=len(myMSMs)-mm)

            myax[pidx].set_ylim(ylim)
            myax[pidx].set_xlim([0, _np.max(time_ax)])
            if legend is not None:
               if isinstance(legend, int):
                  myax[pidx].legend(
                                    frameon=True,
                                    fontsize=fontsize,
                                    framealpha=1.,
                                    loc=legend
                                    ) 
               elif isinstance(legend, str):
                  pass
            if nrows == 1 or pidx[0] == nrows-1:
                myax[pidx].set_xlabel('$t /\ %s$'%tunits, fontsize = fontsize)
                pass


        myfig.tight_layout()
        logging.info('%u frames, %4.1f %s'%(iMSM.lagtime, iMSM.lagtime*dt, tunits))
        if savefig:
           
           fname = 'CK.test.tau.%04.1f.%s.png'%(iMSM.lagtime*dt, tunits[tunits.find('\\')+1:].replace(" ", ""))
           _plt.savefig(fname)
           logging.info('Saved to '+fname)

def plot_bootstrapped_and_MLE_its(BSlist, MLE_its, lagtimes, dt=1.0, 
                             n_ev = 4,
                             tunits = 'frames',
                             show_boot_mean = False,
                             xlim = None,
                             ylim = None,
                             yticks = None,
                             yscale='linear',
                             fontsize = 30,
                             panelsize=10,
                             panel_xy_ratio = 1./2,
                             alpha = .25,
                             title = True,
                             legend_offset = 0,
                             show_outliers = False,
                             cutoff_y_in_tunits = _np.inf, 
                             cutoff_x_in_tunits = None,
                             ylim_outliers = None):

    # Convert to iterable
    if not isinstance(BSlist, list):
        BSlist = [BSlist]
    
    # Get the number of colums and draw figure
    ncol = len(BSlist)        
    myfig, myax = _plt.subplots(1, ncol, sharey='row', figsize=(panelsize*ncol ,panelsize*panel_xy_ratio))
        
    for jj, iBS in  enumerate(BSlist):
        
        # Find the outliers
        iBS.find_outliers_inliers(cutoff_y_in_tunits, cutoff_x_in_tunits = cutoff_x_in_tunits, new_dt = dt)

        # This will be corrected in the future
        maxlag = _np.min((len(lagtimes), iBS.n_lagtimes))
        
        # Be able to handle axes when only one axis is present
        if ncol == 1:
            pidx = 0
            myax = [myax]        
        else:
            pidx = _np.unravel_index(jj, myax.shape)
        
        # Plot!
        for ii in _np.arange(n_ev):
            ## MLE timescales            
            myax[pidx].plot(lagtimes*dt, MLE_its[:,ii]*dt,'-', 
                            color=mycolors[ii], label='$\\tilde{t}_%u$'%(ii+legend_offset))
    
            # Mean of the boot'd model
            if show_boot_mean:
                myax[pidx].plot(iBS.lagtimes*dt, iBS.inlier_mean[:,ii]*dt, '--', 
                                color=mycolors[ii],
                                label='$<t_%u>$'%(ii+legend_offset))
            
            # Errorbars
            upper=(MLE_its[:maxlag,ii]+iBS.inlier_std[:maxlag,ii])
            lower=(MLE_its[:maxlag,ii]-iBS.inlier_std[:maxlag,ii])
            myax[pidx].fill_between(lagtimes[:maxlag]*dt, lower*dt, upper*dt, alpha = alpha, color=mycolors[ii])

        # Print a title for the panel
        if title:
            myax[pidx].set_title('2 traj boot\'d in %u files a $\sim$ %.0e ns \n(%u times - %u outliers with $t_1 > $ %.0e ns) '%
                                 (iBS.nfiles_per_sample, 
                                  iBS.nframes_per_file*iBS.dt, iBS.n_bs, 
                                  iBS.n_outliers, 
                                  iBS.cutoff_y_in_tunits,
                                  ))
        
        # Grey filler
        grey_ax = _np.linspace(lagtimes[0], lagtimes[-1], 100)*dt
        myax[pidx].fill_between(grey_ax, _np.zeros_like(grey_ax), grey_ax, color='gray',zorder=0)

        # Scale
        myax[pidx].set_yscale(yscale, nonposy='clip')
        
        # Limits
        if xlim is not None:
            myax[pidx].set_xlim(xlim)
            
        if ylim is not None:
            myax[pidx].set_ylim(ylim)
        
        # Legend and labels
        myax[pidx].legend(loc=0, fontsize = fontsize, framealpha = 1.0)
        myax[pidx].set_xlabel('$\\tau / %s$'%tunits, fontsize=fontsize)
        myax[pidx].set_xticklabels(myax[pidx].get_xticks(),fontsize=fontsize)
        myax[pidx].set_ylabel('$t_i / %s$'%tunits, fontsize=fontsize)
        if yticks is not None:
           myax[pidx].set_yticks(yticks)

        myax[pidx].set_yticklabels(myax[pidx].get_yticks(),fontsize=fontsize)
            
    myfig.tight_layout()

     # If outliers wanted
    if show_outliers:
        myfig, myax = _plt.subplots(1, ncol, sharey='row', figsize=(panelsize*ncol ,panelsize*panel_xy_ratio))
        for jj, iBS in  enumerate(BSlist):

            if ncol == 1:
                pidx = 0
                myax = [myax]        
            else:
                pidx = _np.unravel_index(jj, myax.shape)

            # Plot the outlier cutoff
            myax[pidx].plot(iBS.lagtimes*dt, _np.ones(iBS.n_lagtimes)*iBS.cutoff_y_in_tunits,'-k', lw=2, label='cutoff outliers') 
            
            # Plot the inliers 
            myax[pidx].plot(iBS.lagtimes*dt, iBS.boot[iBS.inliers, :,0].T*dt,'-', color='blue') 
            if iBS.n_outliers != 0 :
               # Plot the outliers
               myax[pidx].plot(iBS.lagtimes*dt, iBS.boot[iBS.outliers,:,0].T*dt,'-', color='grey') 
            
            myax[pidx].set_yscale(yscale)
            # Limits
            if xlim is not None:
                myax[pidx].set_xlim(xlim)
            if ylim_outliers is not None:
                myax[pidx].set_xlim(ylim_outliers)
            
            myax[pidx].set_xlabel('$\\tau / %s$'%tunits, fontsize=fontsize)
            myax[pidx].set_ylabel('$t_i / %s$'%tunits, fontsize=fontsize)
            myax[pidx].set_title('overlay boot\'d ITS-plots (for $t_0$)')
            myax[pidx].set_xticklabels(myax[pidx].get_xticks(),fontsize=fontsize)
            myax[pidx].set_yticklabels(myax[pidx].get_yticks(),fontsize=fontsize)
        
    myfig.tight_layout()


def plot_EV_sign(MSM,
                 centers,
                 dims,
                 left = True,
                 ncolbins=100,
                 markersize=10,
                 colormap='bwr',
                 n_extrema=3,
                 panelsize = 5,
                 force_equal_axes = True,
                 fontsize = 20):

    """
    Provided with an MSM and a list of indices, produces a (sub)plot where the sign structure (and intensity)
    of the left (or right) eigenvectors can be seen.

    Parameters:
    -----------

    MSM:    pyEMMA's MSM type object

    centers:    _np.ndarray of shape (nclusters, 2) containing the position of the centers

    dims:   iterable with the indices of the left (or two right) eigenvectors to be plotted

    left:   boolean, default = True.
            Wheather to plot left or right eigenvectors

    ncolbins:   int, default 100
                The number of color gradations to use (color resolution, if you will)

    markersize: int, default 1
                The size of the marker for each clustercenter

    colormap:    string, default 'bwr'
                _plt.colormap name. See here for options:
                http://matplotlib.org/examples/color/colormaps_reference.html
                If intensity of the EV is not that important, "seismic" highlights the sign structure
                very clearly, while leaving the near-zero regions in white.

    n_extrema:  The first n argmaxs (and n argmins) will be highlighted in green. Helps in visualizing the
                most likely transition (if left = True)

    panelsize:  int, default 5
                The resulting subplot will be parsed figsize = (len(dims)* panelsize)

    force_equal_axes:   boolean, default = True
                        Froces the subplots to have equal axes, i.e., the distance measured in x and y
                        is the same. If the spread of x and y is not the same, one might get
                        highly rectangular (as opposed to square) panels. Is intended for use with
                        spaces in which the input parameters should have equal variance (e.g. TICA)
    fontsize:   int, default = 20
                will be parsed one for the labels and the titles

    Returns:
    ---------
    fig_handle, ax_handle:    figure and axis handles, to be use further elsewhere

        """


    # Compute the eigenvectors if needed
    MSM.eigenvectors_left()

    assert MSM.nstates == centers.shape[0], "MSM.nstates (%u) and n_centers (%u) are not equal"%(MSM.nstates, centers.shape[0])
    # Choose if right or left eigenvalue simplex
    if left:
        EVs = MSM.eigenvectors_left().T[:,dims]
        label = '$\phi_%u$'
    else:
        EVs = MSM.eigenvectors_right()[:,dims]
        label = '$\psi_%u$'

    nEVs = EVs.shape[1]
    myfig, myax = _plt.subplots(nrows=1,ncols=nEVs, sharey=True, figsize=(panelsize*nEVs, panelsize))
    
    # Create a centers array for the colors 
    colorcenters=_np.linspace(-1, 1.0, ncolbins)

    # Create the color map
    rwb = _plt.get_cmap(colormap)(_np.linspace(0, 1.0, ncolbins))[::-1]

    # Sign structure
    for ii in _np.arange(0,nEVs):
        # Scaled EV so that argmax[_np.abs(EV)]=1
        EVs[:,ii] /= _np.max(_np.abs(EVs[:,ii])) 
  
        # Discretize that scaled EV
        rwb_idx = _np.argmin(_cdist(_np.array(EVs[:,ii]    , ndmin=2).T,
                                  _np.array(colorcenters , ndmin=2).T),1)

        for jj, cc in enumerate(centers):
            myax[ii].plot(cc[0],
                            cc[1],
                            ' o', markeredgecolor='none',
                            color=rwb[rwb_idx[jj]],
			    markersize=markersize)

       # Plot nx argmin and argmax:
        nx = n_extrema
        sort_idx=_np.argsort(EVs[:,ii])
        if ii != 0:
           for mm in (sort_idx[:nx], sort_idx[-nx:]):
               myax[ii].plot(centers[mm,0],centers[mm,1], 'og', markerfacecolor='none', 
                                       markersize=markersize*_np.sqrt(2),
                                       markeredgewidth='3',
                                       markeredgecolor='g')

        # Plot some aids for the quadrants
        myax[ii].plot((_np.min(centers, 0)[0],_np.max(centers,0)[0]),(0,0), '--k')
        myax[ii].plot((0,0),(_np.min(centers, 0)[1],_np.max(centers,0)[1]), '--k')
        
        myax[ii].set_title(label%ii, fontsize = fontsize)
        myax[ii].set_xlabel('IP$_0$', fontsize = fontsize)
        myax[ii].set_xticklabels(myax[ii].get_xticks(), size=fontsize*.75)
        myax[ii].set_yticklabels(myax[ii].get_yticks(), size=fontsize*.75)


        if force_equal_axes:
            myax[ii].set_aspect('equal', adjustable='box-forced')

    myax[0].set_ylabel('IP$_1$', fontsize = fontsize)
        
    myfig.tight_layout()

    return myfig, myax

def plot_EV_simplex(MSM, dims,
                     left = True,
                     dotsize_of_mu = True,
                     mu_factor=1.,
                     n_extrema = None,
                     pad= None,
                     fontsize=20,
                     xlim = None,
                     ylim = None,
                     figsize=(10,10),
                     ):
    """
    Provided with an MSM and a pair of indices, produces a simplex view of the eigenvectors

    Parameters:
    -----------

    MSM:    pyEMMA's MSM type object


    dims:   tuple (i, j), indices of the two left (or two right) eigenvectors to be plotted

    left:   boolean, default = True.
            Wheather to plot left or right eigenvectors

    dotsize_of_mu:  boolean, default = True.
                    Draw the microstate's point in the x,y-plane with a size proprotional
                    to its associated stationary population

    mu_factor:  float, default = 1.
                multiply all mu-values with this float. Useful for cases with few microstates
                (and huge dot-sizes)

    n_extrema:  optional, int. Number of extrema to be returned. Default = None

                Will return the indices for the first n argmax and and n argmin for each EV.
                The returned microstates will be plotted in red, and indexed wrt to the
                LargestConnectedSet, not the full set.
                TODO: think about if it's better a full-set indexing

    pad:  float, default = None, which will add 10% padding space to each axis
                extra-space to be added to the xlim, and ylim values. For beautifying the plot

    fontsize, xlim, ylim, figsize: options for the corresponding _plt. attribute

    Returns:
    fig_handle, ax_handle:    figure and axis handles, to be user further elsewhere

    or
    fig_handle, ax_handle, extrema: same as above, but including the n_extrema that were requested.
                                    n_extrema.shape=[n_extrema*2, 2], where each column contains
                                     [1st argmin, 2nd argmin...nth argmin, nth argmax ...2nd argmax, 1st argmax]
                                     for each EV
    """

    # Get the wanted dims
    (i, j) = dims

    # Compute the eigenvectors if needed
    MSM.eigenvectors_left()

    # Choose if right or left eigenvalue simplex
    if left:
        EVx = MSM.eigenvectors_left().T[:,i]
        EVy = MSM.eigenvectors_left().T[:,j]
        label = 'phi'
    else:
        EVx = MSM.eigenvectors_right()[:,i]
        EVy = MSM.eigenvectors_right()[:,j]
        label = 'psi'

    # Create a pi vector if none was parsed
    if dotsize_of_mu:
        mu = MSM.stationary_distribution
    else:
        mu = _np.ones_like(EVx)
        mu = mu/mu.sum()

    myfig, myax = _plt.subplots(1,1, figsize = figsize )
    # Attention:
    # The "markersize"-variable, is actually the radius (not the size) of the marker.
    # For the *areas* to be proportional, we use hence the square root of the stationary distribution
    rad=_np.sqrt(mu)
    # Make all numbers > 1
    rad/=_np.min(rad)

    # Plot each center
    for ii in _np.arange(len(EVx)):
        myax.plot(EVx[ii],EVy[ii],' ob', markersize=rad[ii]*mu_factor)

    if n_extrema is not None:
        extrema=_np.zeros((2*n_extrema,2))
        for ii, EV in enumerate([EVx,EVy]):
            extrema[:,ii]=_np.hstack((_np.argsort(EV)[: n_extrema],
                                     _np.argsort(EV)[-n_extrema:]))

        for ii, ex in enumerate(_np.reshape(extrema,-1,1,).astype('int')):
            myax.plot(EVx[ex],EVy[ex],' ob', markersize=rad[ex]*mu_factor, color='r')


    label_x = '$\\tilde{\%s}_%u$'%(label, i)
    label_y = '$\\tilde{\%s}_%u$'%(label, j)
    myax.set_xlabel(label_x, fontsize=fontsize)
    myax.set_ylabel(label_y, fontsize=fontsize)

    # Limits
    if xlim is None:
        xlim = myax.get_xlim()
    if ylim is None:
        ylim = myax.get_ylim()

    myax.set_xlim(xlim)
    myax.set_ylim(ylim)
    
    # Plot some visual aids for the quadrants
    if pad is not None:
        assert isinstance(pad,float)
        padx = _np.diff(myax.get_xlim()).squeeze()*pad
        pady = _np.diff(myax.get_ylim()).squeeze()*pad
    else:
        padx = _np.diff(myax.get_xlim()).squeeze()*.1
        pady = _np.diff(myax.get_ylim()).squeeze()*.1

    myax.plot((myax.get_xlim()[0]-padx,myax.get_xlim()[1]+padx),(0,0), '--k')
    myax.plot((0,0),(myax.get_ylim()[0]-pady,myax.get_ylim()[1]+pady), '--k')
    myax.set_xticklabels(myax.get_xticks(), size=fontsize)
    myax.set_yticklabels(myax.get_yticks(), size=fontsize)

    myfig.tight_layout(pad=10)

    _plt.draw()

    if n_extrema is None:
        return myfig, myax
    else:
        return myfig, myax, extrema.astype('int')


def plot_PDF_FES_contour_discr(PDF, xedges, yedges,
                               ncountour=30, regcenters=[],
                               Voronoi=False,
                               axes_equals=False,
                               fontsize=20,
                               xlabel=[],
                               ylabel=[],
                               return_centers=False,
                               return_discretization=False
                               ):

    # Plot PDF and the FES
    myfig, myax = _plt.subplots(2, 2, sharey=True, sharex=True, figsize=(15,15))

    X, Y = _np.meshgrid(xedges[:-1], yedges[:-1])
    # Plot the PDF (upper panels)
    myax[0,0].pcolormesh(xedges, yedges, PDF.T)
    myax[0,1].contour(X, Y, PDF.T, ncountour, zorder=1, colors='k', linestyles='solid', alpha=.75)

    # Plot the FES (lower panels)
    FES=-_np.log(PDF)
    FES[FES==_np.infty]=0
    myax[1,0].pcolormesh(xedges, yedges, FES.T)
    myax[1,1].contour(X, Y, FES.T, ncountour, zorder=1, colors='k', linestyles='solid', alpha=.75)

    if regcenters!=[]:
        # Get some random colors

        if isinstance(regcenters, int):
            # Add the Voronoi-assignments:
            # 1. Get a reggrid for the covered space only:
            space=_np.argwhere(PDF!=0)
            space=_np.vstack((xedges[space[:,0]],yedges[space[:,1]])).T
            # 2. Reduce these space by some reggrid clustering
            regcenters=myregspace(space, 0.015, ncenters=regcenters)
            # 2. Assign each point of that reggrid to the regcenters:
            space_disc=_np.argmin(_cdist(space,regcenters),1)

        elif isinstance(regcenters, _RegularSpaceClustering):
            regcenters = regcenters.clustercenters

        # Generate random colors
        colors = _plt.get_cmap('jet')(_np.linspace(0, 1.0, len(regcenters)))
        _np.random.shuffle(colors)

        for ii, cc in enumerate(regcenters):
            myax[0,1].plot(cc[0],cc[1],'o', color=colors[ii], mec='none', zorder=0)
            myax[1,1].plot(cc[0],cc[1],'o', color=colors[ii], mec='none', zorder=0)

        if Voronoi:
            for ii, thispoint in enumerate(space):
                myax[0,1].plot(thispoint[0],thispoint[1],'o', color=colors[space_disc[ii]], mec='none', zorder=0)
                myax[1,1].plot(thispoint[0],thispoint[1],'o', color=colors[space_disc[ii]], mec='none', zorder=0)

        # Cosmetics
        if axes_equals:

            myax[0,0].set_aspect('equal', adjustable='box-forced')
            myax[0,1].set_aspect('equal', adjustable='box-forced')
            myax[1,0].set_aspect('equal', adjustable='box-forced')
            myax[1,1].set_aspect('equal', adjustable='box-forced')

        myax[0,0].set_title('PDF', fontsize=fontsize)
        myax[0,1].set_title('Discretization (n = %u)'%len(regcenters), fontsize=fontsize)

        myax[1,0].set_title('-log(PDF)', fontsize=fontsize)
        myax[1,1].set_title('Discretization (n = %u)'%len(regcenters), fontsize=fontsize)

        myax[0,0].set_yticklabels(myax[0,0].get_yticks(),fontsize=fontsize)
        myax[1,0].set_yticklabels(myax[1,0].get_yticks(),fontsize=fontsize)
        myax[1,0].set_xticklabels(myax[1,0].get_xticks(),fontsize=fontsize)
        myax[1,1].set_xticklabels(myax[1,1].get_xticks(),fontsize=fontsize)

        if ylabel!=[]:
            myax[0,0].set_ylabel(ylabel, fontsize=fontsize-5)
            myax[1,0].set_ylabel(ylabel, fontsize=fontsize-5)

        if xlabel!=[]:
            myax[1,0].set_xlabel(xlabel, fontsize=fontsize-5)
            myax[1,1].set_xlabel(ylabel, fontsize=fontsize-5)

        myfig.tight_layout()

    if return_centers and not return_discretization:
        return regcenters
    if return_discretization:
        return regcenters, space, space_disc


def plot_hTICA_eigenvalues(lambdas_a,hlambdas = None, m_a = None,
                           xlim = None,
                           xlabel = '$i$',
                           ylabel = '$\lambda_i$' ,
                           fontsize = 40,
                           nice_TeX = True
                           ):
    """
    produces an eigenvalue plot with an overlay of the lambdas of the first round of hTICA.

    Input:

    lambdas_a:  numpy.ndarray of shape (N_a, N_r), containing the eigenvalues of the first round of TICA.
                N_a is the number of hTICA sets and N_r the number of coordinates per set.

    hlambdas:   nump.ndarray of shape ( ) containig the eigenvalues of the second round of TICA

    m_a:        int, default None
                number of EVs kept in for each set in the first round of TICA

    nice_Tex:   boolean, default = True
                allow for fancier labelling, eg. $\ddagger$

    fontsize:   int, default = 40.
                Fontsize for labelling

    xlim, xlabel, ylabel:   default values for the corresponding _plt. attribute

    """

    # Do some nice LaTex plotting
    if nice_TeX:
        _plt.rc('text', usetex=True)
        _plt.rc('font', family='serif')
        #_plt.rcParams['text.latex.unicode']=True
        pass

    _plt.figure(figsize = (15,10))
    if hlambdas is not None:
        l1 = _plt.plot(_np.abs(hlambdas), lw=2)

    l2 = _plt.plot(_np.abs(_np.squeeze(lambdas_a).T),'-', color='gray')

    # Plot a vertical indicator of how many vectors per set were kept
    if m_a is not None:
        _plt.plot([m_a]*20,_np.linspace(0,1,20),'-.k', lw=2)

    if xlim is None:
        _plt.xlim([0, 4*m_a])
    else:
        _plt.xlim(xlim)

    _plt.xticks(fontsize=fontsize, weight='roman' )
    _plt.yticks(fontsize=fontsize)
    if hlambdas is not None:
        _plt.legend((l1[0], l2[0]), ('$\\lambda_i^{\ddagger}$', '$\{\\lambda_i^{(a)\ddagger}\}$'), fontsize=fontsize, frameon=False, numpoints=10, markerscale=10,  )
    else:
        _plt.legend((l2), (['$\{\\lambda_i^{(a)\ddagger}\}$']), fontsize=fontsize, frameon=False, numpoints=10, markerscale=10,  )

    # Labels
    _plt.xlabel(xlabel, fontsize=fontsize)
    _plt.ylabel(ylabel, fontsize=fontsize)

    # This will reset the matplotlib params to their default
    # TODO: save the values prior to exectuion and reset them
    # TODO: GET IT TO WORK!
    if nice_TeX:
        pass


def show_LEVS_from_T(TT,
                     ET = None,
                     connected_set = None,
                     cpi = None,
                     figsize=(10, 10),
                     pad=1,
                     statemax=1,
                     info_text=False,
                     axes_list=None,
                     bar=False,
                     potential = None,
                     equilibrated_pi = None,
                     x_axis = None,
                     fake_renorm = False, 
		     lw = 1):

    # TT=True T
    # ET=Estimated T
    # cpi=pi estimated by counts
    import msmtools
    # Compute the true quantities
    lambdas = _np.abs(msmtools.analysis.eigenvalues(TT))
    its = msmtools.analysis.timescales(TT)
    its = -1./_np.log(_np.abs(lambdas))
    its[0] = _np.inf

    LEVS = msmtools.analysis.eigenvectors(TT, right = False).T
    # Make sure the argmax(_np.abs(LEV)) always has postive value (for visual comparison)
    sign_points = _np.argmax(_np.abs(LEVS),0)
    sign_points = LEVS[[sign_points],[_np.arange(len(LEVS))]]
    LEVS = LEVS / _np.abs(LEVS).sum(0)[_np.newaxis, :]
    LEVS = LEVS * _np.sign(sign_points)
    LEVS[:,0] -= LEVS[:,0].min()
    LEVS[:,0] /= LEVS[:,0].max()
    Tnstates = LEVS.shape[0]

    # Prepare for connectivity issues
    if connected_set is None:
        cset = _np.arange(Tnstates)
    else:
        cset = connected_set

    # Compute the estimated quantities
    if ET is not None:
        Enstates = ET.shape[0]
        ELEVS = msmtools.analysis.eigenvectors(ET, right=False).T
        # Make sure the argmax(_np.abs(LEV)) always has postive value (for visual comparison)
        sign_points = _np.argmax(_np.abs(ELEVS),0)
        sign_points = ELEVS[[sign_points],[_np.arange(len(ELEVS))]]
        ELEVS = ELEVS * _np.sign(sign_points)
        ELEVS = ELEVS / _np.abs(ELEVS).sum(0)[_np.newaxis, :]
        elambdas = _np.abs(msmtools.analysis.eigenvalues(ET))
        eits = _np.abs(msmtools.analysis.timescales(ET))

        if Enstates != Tnstates:
            assert connected_set is not None, "The true MSM and the estimated MSM have different" \
                                              " nstates, yet you did not provide a connected set (%u, %u)"%(Tnstates, Enstates)
        if connected_set is not None:
            assert isinstance(connected_set, _np.ndarray), "connected set is not an _np.ndarray"
            assert connected_set.ndim is 1, "connected set should have dimension one"
            assert connected_set.shape[0] == Enstates, "The length of the connected set does not match the nstates" \
                                                       " of the estimated MSM (%u, %u)"%(connected_set.shape[0], Enstates)
        #assert LEVS.shape[0] == ELEVS.shape[0], 'The MSMs you want to compare have different nstates (%u vs %u)' %(LEVS.shape[0], ELEVS.shape[0])

    if potential is not None:
        assert isinstance(potential, _np.ndarray), "Potential is not an _np.ndarray"
        assert potential.ndim is 1
        assert len(potential) == Tnstates, "Potential array has less elements than nstates (%u<%u)"%(len(potential), nstates)

    # Choose the type of x axis (state indices or 1D-coordinate)
    if x_axis is None:
        myxlabel = 'state index i'
        x = _np.arange(Tnstates)
    else:
        assert isinstance(x_axis, _np.ndarray), "Cannot understand the x-axis you parsed me"
        assert x_axis.ndim is 1, "The x-axis should be a _np.ndarray with ndim = 1, not %u"%x_axis.ndim

        myxlabel = 'x / arb. un.'
        x= x_axis
        if potential is not None:
            assert len(x_axis) == len(potential), "The coordinate and the potential have different lenghts"

    # Create figure
    if axes_list is None:
        myfig, myaxes = _plt.subplots(ncols=1, nrows=statemax, sharex=True, figsize=figsize);
    else:
        myaxes = axes_list

    # Labels for y axis
    myylabel = [('$\phi_{i%u} = \pi$' % 0)]
    for jj in _np.arange(1,statemax):
        myylabel.append('$\phi_{i%u}$'%jj)

    # False renormalization for the true EVs, in case visual comparison is not good
    fac = 1
    if fake_renorm:
        fac = Tnstates*1./Enstates

    ### PLOT
    for ii, iax in enumerate(force_iterable(myaxes)):

        #### PLOT
        ## Choose the type of
        if bar:
            # True vectors
            iax.bar(x[:], fac*LEVS[:, ii], align='center', width=.1, color='g', label='true');
            # Estimated vectors
            if ET is not None:
                iax.bar(x[connected_set].squeeze() - .1, ELEVS[:, ii], align='center', width=.1, color='b',
                               label='estimated');
            # Counts
            if ii == 0 and cpi is not None:
                iax.bar(x[connected_set].squeeze() + .1, cpi / cpi.sum(),
                        align='center', width=.1, color='r', label='counts'
                        )

        else:
            # True vectors
            iax.plot(x[:], fac*LEVS[:, ii], color='g', label='true', lw = lw);
            # Estimated vectors
            if ET is not None:
                iax.plot(x[connected_set], ELEVS[:, ii]/ELEVS[:,ii].max(), color='b', lw = lw, label='estimated');
            # Counts
            if ii == 0 and cpi is not None:
                    iax.plot(x, cpi / cpi.max(),
				    lw = lw,
                             color='r', label='counts'
                            )

        # Y- Axis shaping and labelling
        iax.set_ylim([-_np.max(abs(LEVS[:, ii]*fac) * 1.5),
                      +_np.max(abs(LEVS[:, ii]*fac) * 1.5)])

        iax.set_ylabel(myylabel[ii], fontsize = 40)

        # X axis labelling
        iax.set_xlim((x[:].min()-_np.diff(x)[0]*.5, x[:].max() + _np.diff(x)[0]*.5))

        # Black baseline at zero
        iax.plot(iax.get_xlim(), _np.array([0,0]), color='k')

        # More specialities of the first axis
        if ii == 0:
            iax.set_ylim([0, iax.get_ylim()[1]])
            if potential is not None:
                potential -= potential.min()
                potential /= potential.max()
                potential *= iax.get_ylim()[1]*.8

                iax.fill_between(x, _np.zeros_like(potential), potential,
                                 color = 'gray', alpha = .75, zorder = 10)
            # Dress up
            if equilibrated_pi is not None:
                assert isinstance(equilibrated_pi, _np.ndarray)
                assert equilibrated_pi.shape[1] == 2
                print(fac)
                iax.plot(equilibrated_pi[:,0], equilibrated_pi[:,1]/equilibrated_pi[:,1].max()*iax.get_ylim()[1]*.75, '--k', lw = lw, label = 'eq counts')

        _plt.sca(iax)
        _plt.yticks(fontsize=15)

        # Show the lambdas and its
        if info_text:
            iax.text(iax.get_xlim()[1] +pad/10., + _np.mean(iax.get_ylim()), 'True:\n\n'
                                                                            '$\lambda_{%u}=%14.7f$\n\n'
                                                                            '$t_{%u}={-\\tau} / {log(|\\lambda_{%u}|)}$\n\n$=%10.5f$'% (
										    ii, lambdas[ii], lambdas[ii], ii, its[ii]),
                     fontsize=15, color='green', multialignment='left', va='center')

            if ET is not None:
                iax.text(iax.get_xlim()[1] + pad, + _np.mean(iax.get_ylim()), 'MLE-estimation:\n\n'
                                                                            '$\lambda_{%u}=%14.7f$\n\n'
                                                                            '$t_{%u}={-\\tau} / {log(|\\lambda_{%u}|)}$\n\n$=%10.5f$'% (
                                ii, elambdas[ii], elambdas[ii], ii, eits[ii]),
                                fontsize=15, color='blue', multialignment='left', va='center')

        iax.legend(loc=0, fontsize=15, ncol=2)





    # X-axis shaping and labelling
    iax.set_xlabel(myxlabel, fontsize = 20)
    if x_axis is None:
        iax.set_xticks(x)
        iax.set_xticklabels(iax.get_xticks(), fontsize=25)

    if axes_list is None:
        myfig.tight_layout()

def plot_adaptive(T, MSMs, samples, timescales = None, V = None, x = None):

    if timescales is not None:
        print("True  timescales :", (['%07.1f'%l for l in timescales]))
        print("========================================================")

    for ii, (iMSM, isample) in enumerate(zip(MSMs, samples)):
        __, myax = _plt.subplots(1,2, figsize= (10, 5), sharey = True)
        show_LEVS_from_T(T.transition_matrix, iMSM.transition_matrix, axes_list=myax[0],
                              connected_set = iMSM.active_set,
                              statemax = 1,
                              potential = V,
                              fake_renorm = True,
                              x_axis = x,
                              pad = 1.,
                              info_text = False)

        plot_sample_freq_on_V(isample, V/V.max()*myax[0].get_ylim()[1]*.8, x, ax = myax[1])
        itimescales = -1./_np.log(_np.abs(iMSM.eigenvalues()[1:4]))
        print("Model timescales :", ['%08.4f'%l for l in itimescales], "epoch %02u"%ii)


def plot_sample_freq_on_V(sample, V, x, ax = None):

    samples_unique = _np.unique(sample)
    samples_freq = _np.bincount(sample)[samples_unique]
    samples_and_freqs = _np.vstack((samples_unique, samples_freq))

    if ax is None:
        __, ax = _plt.subplots(1,1, figsize = (10,5 ))

    for (pos, rad) in samples_and_freqs.T:
        ax.plot(x[pos], V[pos], ' or', markersize = _np.sqrt(rad)*5)

    ax.fill_between(x, _np.zeros_like(x), V, color = 'gray', alpha = .75)

def correlation_centers_EVs(MSM, centers,
                            n_feat = 5, n_evs = 5,
                            figsize = None,
                            panelsize = 5,
                            right = True,
                            sharex = False,
                            sharey = False,
                            savefig = None,
                            verbose = False,
                            ):

    """
    Provided with an MSM and the set of clustercenters on which the MSM has been obtained,
    produces scatter plots and linear correlation values between the right (or left) eigenvectors
    and the input features (via their values on the clustercenters)
    Parameters:
    -----------

    MSM:            pyEMMA's MSM type object


    centers:        ndarray(n, m), where n hast to be equal to MSM.nstates

    n_feat, n_evs:  integers, number of features and eigenvectors to display and correlate

    figsize:        specify the desired figsize as a tuple. Default (None) is XXXX

    sharex, sharey: boolean, weather the subplots will be sharing their axes

    panelsize:      size of each panel of the subplot

    right:          boolean, default = True.
                    Wheather to plot left or right eigenvectors


    Returns:
    fig_handle, ax_handle:    figure and axis handles, to be user further elsewhere

    """


    nrows = n_feat
    ncols = n_evs

    if right:
        EVS = MSM.eigenvectors_right()
    else:
        EVS = MSM.eigenvectors_left().T


    if figsize is None:
        figsize = (panelsize*ncols, panelsize*nrows)


    myfig, myax = _plt.subplots(nrows, ncols, figsize=figsize,
                               sharey = sharey, sharex = sharex,
                               )

    for (ev, ip)  in _np.vstack(_np.unravel_index(_np.arange(ncols*nrows),(ncols,nrows))).T:

        x = centers[:,ip]
        y = EVS[:,ev]
        corr = _np.corrcoef(x.T, y = y.T)[0,1]
        mycorr = (((x-x.mean())*(y-y.mean())).sum()/(x.std()*y.std()))/len(x)
        myax[ip,ev].plot(x, y, ' .',
                        label='EV %u vs IP %u\n corr = %4.2f'%(ev,ip, corr))

        if verbose:
            print("mean, std IP %u: %f %f"%(ip, x.mean(), x.std()))
            print("mean, std EV %u: %f %f"%(ev, y.mean(), y.std()))
            print("mycorr ", mycorr)
            print()


        # Cosmetics
        myax[ip,ev].plot(myax[ip,ev].get_xlim(),
                         [0,0],'--k')
        myax[ip,ev].plot([0,0],
                         myax[ip,ev].get_ylim(),'--k')
        if ev==0:
            myax[ip,ev].set_ylabel('$EV$')
        if ip==nrows-1:
            myax[ip,ev].set_xlabel('$IP$')

        myax[ip,ev].legend(framealpha = 1.0)

    myfig.tight_layout()
    if savefig is not None:
        _plt.savefig(savefig)

    return myfig, myax
                                                                            
def plot_EV_histogram(extrema_feat,
                      nEVs=None,
                      nfeat=None,
                      feat_labels=None,
                      vec_labels = None,
                      ref_vals = None,
                      ref_cols=None,
                      panelsize=3,
                      nbins=5,
                      highlight_bimod=True,
                      colors = 'rg',
                      offset = 1,
                      xlim = None,
                      ):
    r"""
    Plots histograms of the value of the given features at the argmin and argmax of an eigenvector.

    Paramters:
    ---------
    extrema_feat: list
        extrema_feat has to contain all the information, and is typically generated :py:func:`myMDvisuals.EVextrema_frames_MSM`

        That means:
            len(extrema_feat)     = number of eigenvectors for which extrema_feat was generated

            len(extrema_feat[0])  = 2*n_extrema of the 0-th eigenvalue
                extrema_feat[0][0]              has the features of the first-th argmin
                extrema_feat[0][1]              has the features of the second-th argmin
                extrema_feat[0][n_extrema]      has the features of the n-th argmin
                extrema_feat[0][-n_extrema]     has the features of the n-th argmax
                extrema_feat[0][-2]             has the features of the 2nd-th argmin
                extrema_feat[0][-1]             has the features of the first-th argmin


            Only the fist extrema points will be considered:
                extrema_feat[0] [0]             has the features of the first-th argmin
                extrema_feat[0][-1]             has the features of the first-th argmax


    """
    # Find out columns and rows
    nrow = nEVs
    if nrow is None:
        nrow = len(extrema_feat)

    ncol = nfeat
    if ncol is None:
        ncol = extrema_feat[0][0].shape[1]

    # Instantiate figure
    myfig, myax = _plt.subplots(nrow, ncol , sharey = False, sharex = 'col',
                               figsize=(panelsize*ncol,panelsize*nrow))

    if _np.ndim(myax) < 2:
        myax = _np.array(myax,  ndmin=2)

    # Loop over EVs
    bimods = []
    for jj, frames in enumerate(extrema_feat[:nrow]):

            features_min = frames[0]
            features_max = frames[-1]

            ibimod = []
            for ii in _np.arange(ncol):

                # Normal plot
                iax = myax[jj, ii]
                iax.hist(features_min[:,ii], bins = nbins, color=colors[0], histtype='stepfilled', alpha=.5, label='argmin')
                iax.hist(features_max[:,ii], bins = nbins, color=colors[1], histtype='stepfilled', alpha=.5, label='argmax')


                # Plot all the references you want
                if ref_vals is not None:
                    for cc, iiref in enumerate(force_iterable(ref_vals[ii])[:ncol]):

                        if ref_cols is None:
                            icol=mycolors[cc]
                        else:
                            icol=ref_cols[cc]
                        iax.plot([iiref,iiref],iax.get_ylim(), '--', color=icol, lw=2, zorder=0)

                # Axes cosmetics
                iax.set_yticklabels('')
                if jj == nrow-1:
                    if feat_labels is None:
                        flabel = 'feature %u'%ii
                    else:
                        flabel = feat_labels[ii]
                    iax.set_xlabel(flabel)

                if ii == 0:
                    if vec_labels is None:
                        ilabel = 'freq in EV %u'%(jj+offset)
                    else:
                        ilabel = vec_labels[jj]

                    iax.set_ylabel(ilabel)
                    iax.legend(fontsize=10, frameon=False)

                # Look if clearly bimodal
                means = [features_min[:,ii].mean(), features_max[:,ii].mean()]
                stds = [features_min[:,ii].std(), features_max[:,ii].std()]
                order = _np.argsort(means)
                bimod = means[order[0]]+stds[order[0]] < means[order[1]]-stds[order[1]]

                if bimod:
                    ibimod.append(ii)
                if not bimod and highlight_bimod:
                    iax.fill_between(iax.get_xlim(),
                                     _np.zeros_like(iax.get_xlim())+iax.get_ylim()[1],
                                     alpha=.5, color='w', zorder=5)

                if xlim is not None:
                    iax.set_xlim(xlim[ii])

            bimods.append(ibimod)

    myfig.tight_layout(pad=3)
    return myfig, myax, bimods

def EVs_as_its(EVs,
               mode = 'ITS',
               lagtimes = None,
               dt=1.0,
               n_ev = None,
               tunits = 'frames',
               xlim = None,
               ylim = None,
               yticks = None,
               yscale='linear',
               fontsize = 20,
               figsize=(10,5),
               marker=''
               ):


    r""" Generates the typical eigenvalue or timescale plots out of eigenvalues (and not its)
    Parameters:
    -----------
        EVs : iterable with the eigenvectors.
        The first index iterates through different *sets* of EVs (eg. different EV sets of different TICA runs)
    """

    if n_ev is None:
        n_ev = len(EVs[0])

    labels = None
    if lagtimes is not None:
        if len(lagtimes) > len(EVs):
            print("len(lagtimes) is larger than to len(EVs) (%u vs %u), will only plot as many EVs sets " \
                  "as there are lagtimes "%(len(lagtimes), len(EVs)))
            lagtimes = lagtimes[:len(EVs)]

        labels = ['lag %u'%lag for lag in lagtimes]

    # Plots!

    # Initialize

    myfig, myax = _plt.subplots(1,1, figsize=figsize)
    if mode is 'EVS':
        for iEVs in EVs:
            myax.plot(iEVs[:n_ev],'-')
            if labels is not None:
                myax.legend(labels, framealpha=1.)

        myax.set_ylabel("$\lambda$", fontsize=fontsize)
        myax.set_xlabel("$i$", fontsize=fontsize)

    elif mode is 'ITS':
        assert lagtimes is not None, "Cannot choose mode ITS without providing lagtimes"
        # In case EVs has some shorter sets of EVs
        evmax = _np.min([len(iEV) for iEV in EVs])
        trunc_EVs = _np.vstack([iEV[:evmax] for iEV in EVs])[:,:n_ev]

        myax.set_ylabel("$t_i$ / %s"%tunits, fontsize=fontsize)
        myax.set_xlabel("$\\tau$ / %s"%tunits, fontsize=fontsize)
        myax.plot(lagtimes*dt, -lagtimes[:, _np.newaxis]*dt/_np.log(_np.abs(trunc_EVs)), marker=marker)

        # Grey filler
        grey_ax = _np.linspace(lagtimes[0], lagtimes[-1], 100)*dt
        myax.fill_between(grey_ax, _np.zeros_like(grey_ax), grey_ax, color='gray',zorder=0)

        # Scale
        myax.set_yscale(yscale, nonposy='clip')

    myax.set_xticklabels(myax.get_xticks(),fontsize=fontsize)
    myax.set_yticklabels(myax.get_yticks(),fontsize=fontsize)
    myax.set_yscale(yscale)
    if yticks is not None:
           myax.set_yticks(yticks)

    # Limits
    if xlim is not None:
        myax.set_xlim(xlim)

    if ylim is not None:
        myax.set_ylim(ylim)

    return myfig, myax
    """
        # Legend and labels
        myax[pidx].legend(loc=0, fontsize = fontsize, framealpha = 1.0)
        myax[pidx].set_xlabel('$\\tau / %s$'%tunits, fontsize=fontsize)
        myax[pidx].set_xticklabels(myax[pidx].get_xticks(),fontsize=fontsize)
        myax[pidx].set_ylabel('$t_i / %s$'%tunits, fontsize=fontsize)


        myax[pidx].set_yticklabels(myax[pidx].get_yticks(),fontsize=fontsize)
    """

def its(its,
        lagtimes = None,
        dt=1.0,
        n_ev = None,
        tunits = 'frames',
        xlim = None,
        ylim = None,
        yticks = None,
        yscale='linear',
        fontsize = 20,
        figsize=(10,5),
        marker=''
        ):


    r""" Generates the typical eigenvalue or timescale plots
    Parameters:
    -----------
        its : iterable with the timescales
        The first index iterates through different *sets* of its
    """

    if n_ev is None:
        n_ev = len(its[0])

    labels = None
    if lagtimes is not None:
        if len(lagtimes) > len(its):
            print("len(lagtimes) is larger than to len(its) (%u vs %u), will only plot as many EVs sets " \
                  "as there are lagtimes "%(len(lagtimes), len(its)))
            lagtimes = lagtimes[:len(its)]

        labels = ['lag %u'%lag for lag in lagtimes]

    # Plots!

    # Initialize

    myfig, myax = _plt.subplots(1,1, figsize=figsize)

    assert lagtimes is not None, "Cannot choose mode ITS without providing lagtimes"
    # In case EVs has some shorter sets of EVs
    evmax = _np.min([len(iEV) for iEV in its])
    trunc_EVs = _np.vstack([iEV[:evmax] for iEV in its])[:,:n_ev]

    myax.set_ylabel("$t_i$ / %s"%tunits, fontsize=fontsize)
    myax.set_xlabel("$\\tau$ / %s"%tunits, fontsize=fontsize)
    myax.plot(lagtimes*dt, -lagtimes[:, _np.newaxis]*dt/trunc_EVs, marker=marker)

        # Grey filler
    grey_ax = _np.linspace(lagtimes[0], lagtimes[-1], 100)*dt
    myax.fill_between(grey_ax, _np.zeros_like(grey_ax), grey_ax, color='gray',zorder=0)

    # Scale
    myax.set_yscale(yscale, nonposy='clip')

    myax.set_xticklabels(myax.get_xticks(),fontsize=fontsize)
    myax.set_yticklabels(myax.get_yticks(),fontsize=fontsize)
    myax.set_yscale(yscale)
    if yticks is not None:
           myax.set_yticks(yticks)

    # Limits
    if xlim is not None:
        myax.set_xlim(xlim)

    if ylim is not None:
        myax.set_ylim(ylim)

    return myfig, myax
    """
        # Legend and labels
        myax[pidx].legend(loc=0, fontsize = fontsize, framealpha = 1.0)
        myax[pidx].set_xlabel('$\\tau / %s$'%tunits, fontsize=fontsize)
        myax[pidx].set_xticklabels(myax[pidx].get_xticks(),fontsize=fontsize)
        myax[pidx].set_ylabel('$t_i / %s$'%tunits, fontsize=fontsize)


        myax[pidx].set_yticklabels(myax[pidx].get_yticks(),fontsize=fontsize)
    """

def plot_feat_histogram_sets(sets,
                             nsets=None,
                             set_sel = None,
                             feat_sel=None,
                             feat_excl=None,
                             feat_labels=None,
                             set_labels = None,
                             set_colors = None,
                             panel_legends=None,
                             sharey=False,
                             log=False,
                             panelsize=3,
                             bins=5,
                             ref_feat=None,
                             ref_cols=None,
                             xlim=None,
                             fontsize=12,
                             rotate_xlabel=0,
                             rotate_ylabel=90,
                             input_ax = None,
                             pad = 3,
                             frames=None,
                             hist_type='hist',  #bincount, #fill
                             **histkwargs
                             ):

    r"""
    Plots histograms of the value of the given features .

    """

    if frames is not None:
        assert len(frames)==len(sets)
        assert [isinstance(ff, int) for ff in frames]
    if feat_labels is not None:
        pass
        #assert len(feat_labels)==sets[0].shape[1], [len(feat_labels),sets[0].shape[1]]

    histkwargs.setdefault('alpha',1.0)
    histkwargs.setdefault('histtype','stepfilled')
    histkwargs.setdefault('bins', bins)

    if "bandwidth" in histkwargs.keys():
        bandwidth = histkwargs["bandwidth"] 
        del histkwargs["bandwidth"] 

    if 'nGMM'  in histkwargs.keys():
        model = GMM(n_components=histkwargs["nGMM"], n_init=3)
        del histkwargs["nGMM"]

    if panel_legends is None:
        panel_legends = _np.tile(None, len(sets)*len(sets[0][0])).reshape(len(sets),-1)

    # Find out columns and rows
    if set_sel is None:
        set_sel = _np.arange(len(sets))
    nrow = len(set_sel)

    if feat_sel is None:
        feat_sel = _np.arange(sets[0].shape[1])
        if feat_excl is not None:
            feat_sel = feat_sel[_np.in1d(feat_sel, feat_excl, invert=True)]
    ncol = len(feat_sel)

    # Instantiate figure
    if input_ax is None:
        myfig, myax = _plt.subplots(nrow, ncol , sharey = sharey, sharex = 'col',
                                   figsize=(panelsize*ncol,panelsize*nrow))
    else:
        myax = input_ax
        myfig = _plt.gcf()

    if _np.ndim(myax) < 2:
        myax = _np.array(myax,  ndmin=2)
        if myax.shape[0] < nrow:
           myax = myax.T

    # Loop over EVs
    bimods = []

    # Assert aptness of reference features:
    if ref_feat is not None:
       assert isinstance(ref_feat, list), "ref_feat has to be a  list containing  2D-arrays"
       assert _np.ndim(ref_feat[0])==1, "each entry of list 'ref_feat' has to be a 1D array of shape (N)"


    if hist_type== 'bincount':
        bc_max = int(_np.max(([sets[ss][:, feat_sel].max() for ss in set_sel])))+1
    for jj, ss in enumerate(set_sel):
            jset = sets[ss]
            ibimod = []
            for ii, ff in enumerate(feat_sel):
                jiset = jset[:,ff]
                if set_colors is None:
                   icol = 'blue'
                else:
                   icol = set_colors[ss]
                #print('nset(jj) %u, nfeat(ii) %u, selected_feat(ff) %u, ncol %u nrow %u'%( jj, ii, ff, ncol, nrow))
                # Normal plot
                iax = myax[jj, ii]
                if hist_type.lower()== 'bincount':
                    iax.bar(_np.arange(bc_max), _np.bincount(jiset.astype("int"), minlength=bc_max), color=icol, label=str(panel_legends[ss,ff]))
                    pass
                elif hist_type.lower().startswith('hist'):
                    #print(jiset.shape, hist)
                    iax.hist(jiset, color=icol, label=str(panel_legends[ss,ff]), **histkwargs)
                elif hist_type.lower().startswith('fill'):
                    ih, ix = _np.histogram(jiset, histkwargs["bins"])
                    iax.fill_between(ix[:-1], ih, color=icol,
                                     #alpha=a
                    )

                else:
                    raise ValueError("What type of histogram is %s" % hist_type)
                if log:
                    iax.set_yscale('log')

                """
                if 'bandwidth' in locals():
                    extrema = _np.array((jfeat[:,ff].min(), jfeat[:,ff].max()))
                    extrema += _np.array(([-1,+1]))*[extrema[1]-extrema[0]]*.5
                    xkde = _np.linspace(extrema[0], extrema[1] , histkwargs["bins"]) 
                    ykde, bw = kde_sklearn(jfeat[:,ff], xkde, bandwidth=bandwidth, cv=5)
                    iax.plot(xkde, ykde, color=icol, zorder=10, label='bw = %f'%bw)
                    iax.legend()
                """
 
                if 'model' in locals():
                    xgrid, dens = oneDsample_2_densityGMM(jiset, model, extrema =xlim[ff], npoints=50)
                    iax.plot(xgrid, dens, color=icol, zorder=10, label='GMM')
                    iax.legend()

                # Plot all the references you want
                if ref_feat is not None:
                    for cc, iref in enumerate(ref_feat):
                         if ref_cols is None:
                             icol=mycolors[cc]
                         else:
                             icol=ref_cols[cc]
                         #iax.plot([iref[ii],iref[ii]],iax.get_ylim(),'--', color=icol, lw=3)
                         #iax.plot([iref[ii],iref[ii]],iax.get_ylim(),'--', color=icol, lw=3)
                         iax.plot(iref[ff],0,'o', color=icol, markersize=20, markeredgecolor='k', alpha=histkwargs['alpha'], zorder=10)


                # Plot one frame for axis
                if frames is not None:
                    iax.axvline(jiset[frames[ss]],
                                color='k',
                                #color=icol
                                )
                # Axes cosmetics
#                iax.set_xticklabels(iax.get_xticks(), [itxt for itxt in iax.get_xticklabels()], fontsize=int(fontsize*.75))

                if xlim is not None:
                    iax.set_xlim(xlim[ff])

                iax.set_yticklabels('')
                if jj == nrow-1:
                    if feat_labels is None:
                        flabel = 'feature %u'%ff
                    else:
                        flabel = feat_labels[ff]
                    if len(flabel)>1000:
                        iax.set_xlabel(flabel, fontsize=8, rotation=rotate_xlabel)
                    else:
                        iax.set_xlabel(flabel, fontsize=fontsize, rotation=rotate_xlabel)

                if ii == 0:
                    if set_labels is None:
                        ilabel = 'freq in set %u'%(ss)
                    else:
                        ilabel = set_labels[ss]

                    iax.set_ylabel(ilabel, fontsize=fontsize, rotation=rotate_ylabel, va="center", ha="right")
                if (panel_legends != _np.array(None)).all():
                    iax.legend(fontsize=fontsize, frameon=False)

    myfig.tight_layout(pad=pad)
    return myfig, myax

def relabel_axes(ax, fontsize=20, x=True, y=True, remove_labels_and_tics=False, n_freq=1):

    
    if isinstance(ax, _asp.Axes):
       ax = _np.array(ax)
      
       
    for iax in ax.flat:
        if remove_labels_and_tics:
            iax.set_xlabel('')
            iax.set_ylabel('')
            for tl in iax.get_xticklabels() + iax.get_yticklabels():
                tl.set_visible(False)
        else:
            if x:
                iax.set_xlabel(iax.get_xlabel(), fontsize=fontsize)
                ticks = []
                labels = []
                for ii in _np.arange(len(iax.get_xticklabels()))[::n_freq]:
                    ticks.append(iax.get_xticks()[ii])
                    labels.append(silent_list(iax.get_xticklabels()[ii]))

                iax.set_xticklabels(ticks, fontsize=fontsize)
                iax.set_xticks     (ticks)
            if y:
                iax.set_ylabel(iax.get_ylabel(), fontsize=fontsize)
                ticks = []
                labels = []
                for ii in _np.arange(len(iax.get_yticklabels()))[::n_freq]:
                    ticks.append(iax.get_yticks()[ii])
                    labels.append(silent_list(iax.get_yticklabels()[ii]))

                iax.set_yticklabels(ticks, fontsize=fontsize)
                iax.set_yticks     (ticks)

def back_drop(iax, border=None, extent=None, zorder=10  ):

    if border is None:
        if extent is None:
            iax.vlines(0, *iax.get_ylim(), linestyle='--', zorder=zorder)
            iax.hlines(0, *iax.get_xlim(), linestyle='--', zorder=zorder)
        else:
            iax.vlines(0, *extent[:2], linestyle='--', zorder=zorder)
            iax.hlines(0, *extent[-2:], linestyle='--', zorder=zorder)
    else:
        if extent is None:
            iax.contour(border.T, levels=[0], colors=['k'],zorder=-1)
        else:
            iax.contour(border.T, levels=[0], colors=['k'], extent=extent,zorder=-1)

def empty_legend(iax, legend_list, color_list='w', marker_list=' ', handlelength=0, **legendkwargs):
    assert len(legend_list) == len(color_list)
    lines = []
    for color,marker in zip(color_list ,marker_list):
        lines.append(_plt.Line2D([],[], color=color, marker=marker))
    
    iax.legend(lines,legend_list, handlelength=handlelength, **legendkwargs )

def relabel_colrow_subplots(myax, fontsize=15, labelsx=None,
                            labelsy=None, mirror_xlabels=False, mirror_ylabels=False):
    r""" Will relabel the borders of a subplot with labels only on the outer borders
    """
    
    assert isinstance(myax, _np.ndarray)
    assert myax.ndim==2
    assert isinstance(myax[0,0], _asp.Axes)

    nrow, ncol = myax.shape
    dims  =    {"x":ncol,    "y":nrow}
    labels =   {"x":labelsx, "y":labelsy}
    axes =     {"x":myax[-1,:], "y":myax[:,0]}
    
    mirrors =  {"x":mirror_xlabels, "y":mirror_ylabels}
    axes_m = {"x":myax[0,:], "y":myax[:,-1]}
    pos_m ={"x":'top', "y":'right'} 
    
    for key in ["x","y"]:
        if labels[key] is None:
            labels[key]=[key+'/ arb. u.']*dims[key]
     #   else:
     #       assert isinstance(labels[key],list)  ## Better ask for forgiveness...
     #       assert len(labels[key])==dims[key]
    
        for iax, ilab  in zip(axes[key], labels[key]):
            getattr(iax,'set_%slabel'%key)(ilab, fontsize=fontsize)
        
        if mirrors[key]:                    
            for iax, ilab in zip(axes_m[key], labels[key]):
                getattr(iax,'set_%slabel'%key)(ilab,fontsize=fontsize)    
                getattr(iax,'%saxis'%key).set_label_position(pos_m[key]) 
                getattr(iax,'%saxis'%key).set_ticks_position('both') 

def plot_histodd(H, extent=None, only_upper_triangle=True, return_figNaxes=True, log=False, ndims_histo=None,
                 figsize=(20,20), nrows=None, triang=None,
                 panelsize=0, diag_PDF=True, log_PDF=False, log_PDF_colors=None,
                 **contourf_kwargs):
    r"""H has to be a dictionary of dictionaries. H[0][1], H[0][2]... etc"""
    
    assert isinstance(only_upper_triangle, bool)
    assert isinstance(H, dict)
    if ndims_histo is None:
       ndims_histo = len(H.keys())+1
    else:
       assert isinstance(ndims_histo, int)
       assert ndims_histo <= len(H.keys()), (ndims_histo, len(H.keys()))
    
    if nrows is None:
       nrows = ndims_histo-1

    ncols = ndims_histo

    print(nrows, ncols, ndims_histo)

    if diag_PDF:
        nrows +=1
    print(nrows, ncols, ndims_histo)

    if panelsize != 0:
        figsize=(ncols*panelsize, nrows*panelsize)
    # Create figure
    myfig, myax = _plt.subplots(nrows, ncols, figsize=figsize, sharex='col', sharey='row')
 
    myax = _np.array(myax, ndmin=2)
    # Manage the most important variable: pairs
    triu_idxs_coord = _np.vstack(_np.triu_indices(myax.shape[0],  m=myax.shape[1], k=1)).T
    tril_idxs_coord = _np.vstack(_np.tril_indices(myax.shape[0],  m=myax.shape[1], k=-1)).T

    triu_axes = [iax for iax in _np.triu(myax, k=1).flatten()  if iax!=0]
    tril_axes = [iax for iax in _np.triu(myax, k=-1).flatten() if iax!=0]

    if triang is not None and triang.lower() == 'upper':
       pairs = triu_idxs_coord
       axes =  triu_axes
    elif triang is not None and triang.lower() == 'lower':
       pairs = tril_idxs_coord
       axes = tril_axes
    elif triang is None:
       pairs = _np.vstack((triu_idxs_coord, tril_idxs_coord))
       axes = triu_axes



    for ii,jj  in pairs:
        iax = myax[ii, jj]

        if ii < jj:
           iH = H[ii][jj][:,:]
           am_I_in_triup = True
        else:
           iH = H[jj][ii].T[:,:]
           am_I_in_triup = False
        if log:
            iH = -_np.log(iH)

        if extent is None:
           iextent = [0, iH.shape[0], 0, iH.shape[1]]
        else:
           iextent = extent[[jj,ii]].flatten()# Note how H is not H.T and hence extent  is jj,ii NOT ii,jj

        if only_upper_triangle:
            if am_I_in_triup:
                iax.contourf(iH,
                             extent=iextent,
                             **contourf_kwargs
                             )
        else:
            iax.contourf(iH, extent=iextent, **contourf_kwargs)
        back_drop(iax, extent=iextent[::-1])  # Here we DO reverse extent back to ii,jj

    if diag_PDF:
        for ii in range(myax.shape[0]):
            iax = myax[ii,ii]
            try:
                iH  = H[ii][ii+1].sum(-1)
            except KeyError:
                assert ii == ndims_histo - 1
                iH = H[ii-1][ii].sum(0)
                pass
                # Shift to zer  o
            if log_PDF:
                iH = _np.log(iH)
            iH -= iH[~_np.isinf(iH)].min()
            # normalize to have max=hrange/2
            iH /= _np.abs(iH[~_np.isinf(iH)].max())
            iH *=  -_np.abs(_np.diff(iax.get_xlim()))/2
            # Shift to the rightermost point
            iH += iax.get_xlim()[1]
            ix = _np.linspace(*extent[ii], num=len(iH))
            icol = 'r'
            if log_PDF_colors is not None:
                icol = log_PDF_colors[ii]
            iax.plot(iH, ix, icol, lw=4) # better this way than to instantiate a new axis

    myfig.tight_layout()

    if return_figNaxes:
        return myfig, myax

@deprecated('Use spectrum_model instead')
def TICA_spectrum(*args, **kwargs):
    spectrum_model(*args, **kwargs)

def spectrum_model(imodel, dt = 1, ax=None, lowdim=50, color='b',
                   exclude=None,
                   legend=None,
                   unit='frames',
                   panelsize=5,
                   return_stuff = True,
                   logscales=True,
                   xlabels=True,
                   absolute_eigenvalues=False):


    nplots = 5
    if exclude is None:
        exclude = []
    else:
        nplots -= len(exclude)

    if ax is None:

       myfig, myax = _plt.subplots(1,nplots, sharex=False, sharey=False,
                                   figsize=(panelsize*nplots, panelsize),
                                   )
       if not isinstance(myax, _np.ndarray):
           myax = [myax]
    else:
        assert isinstance(ax, _np.ndarray)
        assert isinstance(ax.flatten()[0], _asp.Axes)
        myax = ax.flatten()
        myfig = _plt.gcf()


    eigenvalues = imodel.eigenvalues
    if absolute_eigenvalues:
        eigenvalues = _np.abs(eigenvalues)

    timescales = imodel.timescales
    try:
        eigenvalues = eigenvalues()
        timescales  = timescales()
    except:
        pass

    try:
        cumvar = imodel.cumvar
    except:
        cumvar = _np.cumsum(imodel.eigenvalues()**2)
        cumvar /= cumvar[-1]
 
    lowdim = _np.min((lowdim, eigenvalues.shape[0]-1))

    cc = 0
    if 'lambdas' not in exclude:
        myax[cc].plot(eigenvalues[:lowdim], 'o', color=color, label=legend, markeredgecolor='none')
        if xlabels:
            myax[cc].set_xlabel('$i$')
        myax[cc].set_ylabel('$\lambda_i$')
        cc += 1

    if 'its' not in exclude:
        myax[cc].plot(timescales[:lowdim]*dt, 'o', color=color, label=legend, markeredgecolor='none')
        myax[cc].set_ylabel('$t_i\ / %s $'%unit)
        if xlabels:
            myax[cc].set_xlabel('$i$')
        if logscales:
            myax[cc].set_yscale('log')
        cc += 1

    if 'its_gap' not in exclude:
        myax[cc].plot(timescales[:lowdim]/timescales[1:lowdim+1],'-o', color=color, label=legend, markeredgecolor='none')
        myax[cc].set_ylabel('$\\frac{t_i}{t_{i+1}}$')
        if xlabels:
            myax[cc].set_xlabel('$i$')
        if logscales:
            myax[cc].set_yscale('log')
        cc += 1

    if 'its_vs_its_gap' not in exclude:
        myax[cc].plot(timescales[:lowdim]/timescales[1:lowdim+1],
                      timescales[:lowdim]*dt,
                      ' o', color=color, label=legend, markeredgecolor='none')
        if logscales:
            myax[cc].set_xscale('log')
            myax[cc].set_yscale('log')
        myax[cc].set_ylabel('$t_i / %s $'%unit)
        if xlabels:
            myax[cc].set_xlabel('$\\frac{t_i}{t_{i+1}}$')
        cc += 1

    if 'cumvar' not in exclude:
        myax[cc].plot(cumvar[:lowdim],'-o', color=color, label=legend, markeredgecolor='none')

    if legend is not None:
       [iax.legend(loc=0, fontsize=12) for iax in myax[:]]

    if return_stuff:
        return myfig, myax
    else:
        _plt.show()



def plot_N_2D_histograms(Histo, extent, labels=None,
                         only_upper_triangle=False,
                         title=None, ndims_histo=None, panelsize=8, nrows=None, log=False, triang=None,
                         diag_PDF=True, log_PDF=False, log_PDF_colors=None,
                        **contourf_kwargs):
    r"""H has to be a dicctionary of dictionaries H[0][1] etc etc"""
    
    assert isinstance(Histo, dict)
    

    myfig, myax = plot_histodd(Histo,
                               log = log,
                               extent=extent,
                               only_upper_triangle=only_upper_triangle,
                               panelsize=panelsize,
                               ndims_histo=ndims_histo, figsize=(20,20), nrows=nrows, triang=triang,
                               diag_PDF=diag_PDF, log_PDF=log_PDF, log_PDF_colors=log_PDF_colors,
                              **contourf_kwargs)
    ndims_histo = myax.shape[0]
    if labels is not None:            
        labelsx = labels[:ndims_histo+2] #??? Don't ask me about this two here, please, it's kinda embarassing
        labelsy = labels[:ndims_histo+1]
        if nrows is not None:
           labelsy = labelsy[:nrows]
        relabel_colrow_subplots(myax, fontsize=25,
                                labelsx=labelsx, 
                                labelsy=labelsy, 
                                mirror_xlabels=True,
                                mirror_ylabels=True,
                               )

    if title is not None:
        assert isinstance(title, str)
        myfig.suptitle(title, fontsize=40)
        
    myfig.tight_layout()
    myfig.subplots_adjust(top=.90)
    
    return myfig, myax

def oneDsample_2_densityGMM(sample, gmm, extrema=None, npoints=50, return_parameters=False):
    r"""take a 1D array and GMM and return the fitted density
    on a grid. If extrema is none, 
    grid=np.linspace(sample.min(), sample.max(), nbins))"""
    
    if extrema is None:
        grid = sample.min(), sample.max()
    else:
        grid = extrema[0], extrema[1]
    grid = _np.linspace(*grid, num=npoints)
   
    gmm.fit(_np.array(sample, ndmin=2).T)    
    params = {}
    for var in ['means', 'weights', 'covars']:
        params[var] = getattr(gmm, var+'_')
    if return_parameters:
       return grid, _np.exp(gmm.score(_np.array(grid, ndmin=2).T)), params
    else:
       return grid, _np.exp(gmm.score(_np.array(grid, ndmin=2).T))

def NDsample_2_densitiesGMM(samples, gmm, extrema=None, npoints=50, selection=None, ndebug=None, return_parameters=False):
    r"""given an n-dimensional sample (n, nsamples),
    fit a GMM to each of them and return a densities
    
    if extrema is None, extrema will be (sample.min(), sample.max()) 
    for each n-dimension
    """
    if extrema is None:
        extrema = _np.vstack((_np.vstack([ifeat for ifeat in samples]).min(0),
                             _np.vstack([ifeat for ifeat in samples]).max(0))).T
    
    
    if selection is None:
        selection = _np.arange(samples[0].shape[1])

    if ndebug is None:
        ndebug = len(selection)
    
    print(selection)
    dens = _np.zeros((len(samples), ndebug, npoints))
    params  = []
    for ii, iset in enumerate(samples):
        print(ii, iset[:,selection].shape)
        _stdout.flush()
        iparams = []
        for jj, r in enumerate(iset[:, selection].T[:ndebug]):
            grid, dens[ii, jj, :], jparam = oneDsample_2_densityGMM(r, gmm, extrema=extrema[selection][jj], npoints=npoints, return_parameters=True)
            iparams.append(jparam)
            #print(ii, jj, r.shape, len(iparams))
        params.append(iparams)
        #print()
    if return_parameters:
       return dens, params
    else:
       return dens

def add_snapping_positions(fig, pos, guides=True, return_widget=False):
    r"""Given a figure object and a set of snapping positions, include the positions in
    the figure so that interactive "snapping" of the mouse pointer occurs in the original 
    figure """
    
    from IPython.display import display as ipydisplay
    from scipy.spatial import cKDTree
    from matplotlib.widgets import AxesWidget
    if _plt.get_backend() != 'nbAgg':
       raise NotImplementedError('%s has to be called from an ipython notebook that is already using'%add_snapping_positions.__name__,
                                 ' the "notebook" backend of matplotlib. Use the magic "%matplotlib notebook" for this purpose.')
       
       print("was here")

    x, y = pos[:,0], pos[:,1]
    kdtree = cKDTree(_np.vstack((x, y)).T)
    
    #display(fig[0])
    ax = fig.axes[0]
    
    if guides:
        lineh = ax.axhline(ax.get_ybound()[0], c="k", linestyle='--')
        linev = ax.axvline(ax.get_xbound()[0], c="k", linestyle='--')
    dot, = ax.plot([],[], 'o', c='red', ms=10, zorder=2*len(x)+1)

    def onclick(event):
        try:
            linev.set_xdata((event.xdata, event.xdata))
            lineh.set_ydata((event.ydata, event.ydata))
        except:
            pass
        _, index = kdtree.query(x=[event.xdata, event.ydata], k=1)
        dot.set_xdata((x[index]))
        dot.set_ydata((y[index]))
        
    
    axes_widget = AxesWidget(ax)
    axes_widget.connect_event('button_release_event', onclick)
    
    if return_widget:
        return axes_widget

def plot_length_distro(lengths, dt=1, ax=None, title=None):
    if ax is None:
        ax = _np.arange(1, _np.max(lengths)+2)
    if title is not None:
        _plt.title(title)
    
    count = _np.bincount(lengths, minlength=ax[-1])
    cumsum = _np.cumsum(count*ax)/_np.sum(count*ax)
    halfpoint = _np.argmin(_np.abs(cumsum-.5))    
    _plt.plot(ax*dt, count*ax*dt, label='amount of data \n in trajs this long')
    _plt.legend(loc=0, fontsize=8)
    ax2 = _plt.gca().twinx()
    _plt.sca(ax2)
    _plt.plot(ax*dt, cumsum, 'r', zorder=10, label='CDF(t) blue curve')
    _plt.fill_between(ax[:halfpoint]*dt, cumsum[:halfpoint], alpha=.50, color='r', label='50% of the data')
    _plt.legend(loc=4, fontsize=8)


def core_tracker_plot(xvalues, colors, n_cores, n_interp=0, iax=None):

    icol = colors
    if iax is None:
        _plt.figure()
        iax = _plt.gca()

    _plt.sca(iax)
    _plt.axvline(xvalues[0], color=icol[0])
    _plt.axvline(xvalues[-1], color=icol[-1])

    if n_interp == 0:
        n = 1
    else:
        n = n_interp

    jj = 0
    for cc in _np.arange(n_cores)[1:-1]:
        _plt.axvspan(xvalues[(jj + 1) * n],
                     xvalues[(jj + 2) * n], alpha=0.5,
                     color=icol[cc]
                     )
        jj += 2

    return _plt.gca()

def CustomCmap(from_rgb,to_rgb):
    from matplotlib.colors import LinearSegmentedColormap
    # from color r,g,b
    r1,g1,b1 = from_rgb

    # to color r,g,b
    r2,g2,b2 = to_rgb

    cdict = {'red': ((0, r1, r1),
                   (1, r2, r2)),
           'green': ((0, g1, g1),
                    (1, g2, g2)),
           'blue': ((0, b1, b1),
                   (1, b2, b2))}

    cmap = LinearSegmentedColormap('custom_cmap', cdict)
    return cmap

def panelize_abc(myax_iterable, fac_x=None, fac_y=None, fontsize=None, abc=None, **legendkwargs):
    r"""
    if any fac_x and fac_y is None, panelize_abc will just use "empty_legend" under the hood
    :param myax_iterable:
    :param fac_x:
    :param fac_y:
    :param fontsize:
    :param abc:
    :param legendkwargs:
    :return:
    """
    if abc is None:
        abc='abcdefghijklmnopqrstuvwxyz'

    from matplotlib import rcParams as _rcParams
    if fontsize is None:
        fontsize= _rcParams["font.size"]

    for letter, iax in zip(abc, myax_iterable):
        if fac_x is None or fac_y is None:
            empty_legend(iax, ['%s)' % letter], fontsize=fontsize, **legendkwargs)
        else:
            x, y, = iax.get_xlim(), iax.get_ylim()
            dx, dy = _np.diff(x), _np.diff(y)
            iax.text(x[0] + dx * fac_x, y[0] + dy * fac_y, '%s)'%letter, fontsize=fontsize)
        #iax.text(x,y,'%s)'%letter, fontsize=fontsize, va='middle', ha='left', **textkwargs)


def plot_dtraj_labels(idtraj, iMSM, iax=None, t=None, unique=False):
    if iax is None:
        iax = _plt.gca()
    if t is None:
        t = _np.arange(len(idtraj))
    if unique:
        pass
        states, kdtraj = _np.unique(idtraj, return_inverse=True)
        iax.plot(t, kdtraj)
    else:
        iax.plot(t, iMSM._full2active[idtraj])
        iax.set_yticks(_np.arange(iMSM.nstates))
        iax.set_yticklabels(iMSM.active_set)
