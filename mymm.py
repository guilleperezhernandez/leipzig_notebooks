import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from scipy import optimize as op
import sys
from pyemma import msm


def get_countmatrix(r, microstates=None, lag=1):
    # print "Estimating with lag",lag

    if microstates == None:
        r = microstates2indeces(r)
        microstates = np.unique(r)

    nstates = len(microstates)
    nt = len(r)
    c = np.zeros((nstates, nstates))
    for t in range(0, nt - lag):
        i = r[t]
        j = r[t + lag]
        c[i, j] += 1

    return c


def countmatrix2transitionmatrix_rev_est_JCP2011(c, niter=10, verbose=False, conv=False):
    n = len(c)

    # if verbose:
    #        print c.sum(1)/c.sum()

    x = np.zeros((n, n))
    xi = np.zeros(n)
    ci = np.zeros(n)
    piconv = np.zeros((n, niter))

    for i in range(0, n):
        for j in range(i, n):
            x[i, j] = c[i, j] + c[j, i]
            x[j, i] = x[i, j]
        xi[i] = sum(x[i, :])
        ci[i] = sum(c[i, :])

    for k in range(0, niter):
        #     (2.1)
        for i in range(0, n):
            x[i, i] = c[i, i] * (xi[i] - x[i, i]) / (ci[i] - c[i, i])
            xi[i] = sum(x[i, :])
            #     (2.2)
        for i in range(0, n - 1):
            for j in range(i + 1, n):
                a = ci[i] - c[i, j] + ci[j] - c[j, i]
                b = (ci[i] * (xi[j] - x[i, j]) + ci[j] * (xi[i] - x[i, j])
                     - (c[i, j] + c[j, i]) * (xi[i] + xi[j] - 2 * x[i, j]))
                C = -(c[i, j] + c[j, i]) * (xi[i] - x[i, j]) * (xi[j] - x[i, j])
                d = b ** 2 - 4 * a * C
                x[i, j] = (-b + np.sqrt(d)) / (2 * a)
                x[j, i] = x[i, j]

            xi[i] = sum(x[i, :])
        if conv:
            piconv[:, k] = get_pi_from_T(np.dot(np.diag(1. / xi), x))

        if verbose:
            #            print piconv[:,k]
            print(k)
            sys.stdout.flush()

    xi = np.diag(1. / xi)
    T = np.dot(xi, x)
    if conv:
        return T, piconv
    else:
        return T


def microstates2indeces(r):
    b = np.copy(r)
    old = np.unique(b)
    j = 0
    for i in old:
        b[mlab.find(r == i)] = j
        j = j + 1
    return b


def sort_T(A):
    E, V = np.linalg.eig(np.transpose(A))
    idx = np.argsort(abs(E))
    idx = idx[::-1]
    A = A[idx, :]
    A = A[:, idx]
    return A


def get_pi_from_T(A):
    # Pi
    E, V = np.linalg.eig(np.transpose(A))
    idx = np.argsort(abs(E))
    idx = idx[::-1]
    pi = V[:, idx[0]]
    pi = pi / np.sum(pi)
    return pi


def get_lambdas_from_T(A):
    # Pi
    E, V = np.linalg.eig(np.transpose(A))
    idx = np.argsort(abs(E))
    idx = idx[::-1]
    l = np.abs(E[idx])
    return l


def get_LEVS_from_T(A, norm=False):
    # Pi
    E, V = np.linalg.eig(np.transpose(A))
    idx = np.argsort(abs(E))
    idx = idx[::-1]
    LEV = V[:, idx]
    if norm:
        LEV = LEV / np.tile(np.sum(np.abs(LEV), axis=0), (len(LEV), 1))
    return LEV


def check_stoc_rev(A):
    print('row sums')
    print(A.sum(1))

    # Pi
    pi = get_pi_from_T(A)
    print('pi = ', pi)

    # Detailed balance
    B = np.diag(pi) * A;
    print('norm of pi*P-(pi*P)\' ', np.linalg.norm(B - np.transpose(B)))


def get_ITS_from_T(A, tau=1):
    E, V = np.linalg.eig(np.transpose(A))
    idx = np.argsort(abs(E))
    idx = idx[::-1]
    E = E[idx]
    t = -tau / np.log(abs(E[1:]))
    return t


def waiting_times(T, ri, niter, tmax = int(1e5), roundtrip = False):
    t = []

    for k in np.arange(0, niter):
        r = np.copy(ri)
        nt = 0
        dtraj = [ri]
        for tt in range(tmax):
            d = np.random.rand()
            line = T[r, :]
            j = 0
            nt = nt + 1

            while np.sum(line[:j + 1]) <= d:
                j = j + 1

            dtraj.append(j)
            # If we're not on a round-trip, it's safe to break here
            if j != ri and not roundtrip:
                break

            # If we're round-trip, we break here
            if r != ri and ri == j and roundtrip: #actually, only in roundtrip we would reach this point...
                break

            # Otherwise, just update
            r = j

        # Append only of we did not exhaust tmax
        if tt < tmax-1:
            t.append(nt)
        else:
            if not roundtrip:
                print("iteration %u did not leave state %u in %u steps"%(k, ri, tmax))
            else:
                print("iteration %u did not leave and return to state %u in %u steps"%(k, ri, tmax))
    return np.array(t), dtraj


# Define the exponential function
def myexp(x, N, k):
    y = N * np.exp(-x / k)
    return y


def myexp2(x, N1, N2, k1, k2):
    return N1 * np.exp(-x / k1) + N2 * np.exp(-x / k2)


def myexpN(x, params):
    myexpN = []
    Ns = params[:len(params) / 2]
    ks = params[len(params) / 2:]
    print(Ns)
    print(ks)


# return N1*np.exp(-x/k1)+N2*np.exp(-x/k2)

def fit_2_multi_exp(f, nexp=1, return_fit=False):
    N = np.random.rand(nexp) * np.max(f)
    N[0] = f[0];
    tau = np.random.rand(nexp) * len(f) / 10
    p0 = [N, tau]
    if nexp == 1:
        popt, pcov = op.curve_fit(myexp, np.arange(0, len(f)), f, p0=p0)
        fit = myexp(np.arange(0, len(f)), popt[0], popt[1])
    elif nexp == 2:
        popt, pcov = op.curve_fit(myexp2, np.arange(0, len(f)), f, p0=p0)
        fit = myexp2(np.arange(0, len(f)), popt[0], popt[1], popt[2], popt[3])

    if return_fit:
        return popt, pcov, fit
    else:
        return popt, pcov


def initialize(A, nt, niter):
    # Prepare propability vectors
    pt = np.zeros((len(A), nt))

    # Prepare convergence vector
    conv = np.zeros(nt)

    # Prepare the ranges of the decay rates
    popt = np.zeros((2, niter))

    return pt, popt, conv


def MarkovChain(T, nt, *rinit):
    nt = int(nt)
    # Allocate
    r = np.zeros(nt, 'int')

    # Decide how to initialize
    if not rinit:
        rinit = np.random.randint(0, len(T))
        print("Randomized initial state")

    # Initalize
    r[0] = np.array(rinit)

    # Generate a Markov-Chain
    for t in np.arange(1, nt):
        d = np.random.rand()
        line = T[r[t - 1], :]
        j = 0
        while np.sum(line[:j + 1]) <= d:
            j = j + 1
        r[t] = j
    return r


def show_convergence(A, niter, nt, inset=5):
    pt, popt, conv = initialize(A, nt, niter)
    pi = get_pi_from_T(A)
    myfig, myaxes = plt.subplots(nrows=3, ncols=2, figsize=(10, 10), sharey='row', sharex='col')
    cols = 'brgc'

    for k in np.arange(0, niter):
        # Randomly initialize the populations
        p = np.random.rand(len(A), 1)
        p = p / p.sum()
        pt[:, 0] = p.transpose()
        conv[0] = np.linalg.norm(pt[:, 0] - pi)
        for t in np.arange(1, nt):
            pt[:, t] = np.dot(np.transpose(pt[:, t - 1]), A)
            conv[t] = np.linalg.norm(pt[:, t].transpose() - pi)

        ## All the following loop is crap because I don't know how to reset colors
        # Populations
        myaxes[0, 0].plot(np.arange(0, nt), np.transpose(pt));
        myaxes[0, 1].plot(np.arange(0, nt), np.transpose(pt));

        # Difference with pi
        myaxes[1, 0].plot(np.arange(0, nt), conv);
        myaxes[1, 1].plot(np.arange(0, nt), conv);

        # Difference with pi-normalized to start at one
        myaxes[2, 0].plot(np.arange(0, nt), conv / conv[0]);
        myaxes[2, 1].plot(np.arange(0, nt), conv / conv[0]);

        # Fit the exponential decay
        N = conv[0];
        tau = 1
        popt[:, k], pcov = op.curve_fit(myexp, np.arange(0, nt), conv, p0=[N, tau])
        print('mono-exp-decay to equilibrium N,exp(t/ti)=%5.3f exp(t/%5.3f). Final PDF' % (
        popt[0, k], popt[1, k]), np.array2string(pt[:, -1]))


    # Stuff to be done after the iterations
    # Mark the final pi-values
    pi = get_pi_from_T(A)
    for ll in np.arange(0, len(A)):
        label = '$\pi_%u$' % ll
        myaxes[0, 1].text(nt + .01 * nt, pi[ll], label, fontsize=25)

    # Y-labels and limits
    myaxes[0, 0].set_ylim([0, 1]);
    myaxes[0, 0].set_ylabel('p$_i$', fontsize=40);
    myaxes[1, 0].set_ylabel('$||{\pi}-\mathbf{p}(t)||$', fontsize=25)
    myaxes[2, 0].set_ylabel('$[||{\pi}-\mathbf{p}(t)||]_{t=0}$', fontsize=25)
    # X-labels and limits
    myaxes[2, 0].set_xlim([0, inset])
    myaxes[2, 0].set_xlabel('time / frames', fontsize='xx-large');
    myaxes[2, 1].set_xlabel('time / frames', fontsize='xx-large');
    # Show some text
    text = '$\sim N e^{-\\frac{t}{%6.3f}}$' % np.average(popt[1, :])
    myaxes[2, 1].text(nt / 4, .5, text, fontsize='60', backgroundcolor='white')

    myfig.tight_layout()


def JHPfourwell(x,
                N1 = 0.8,
                N2 = 0.2,
                N3 = 0.5):
    y = 4 * (
    x ** 8 +
    N1  * np.exp(-80 * (x - 0.0) ** 2) +
    N2  * np.exp(-80 * (x - 0.5) ** 2) +
    N3  * np.exp(-40 * (x + 0.5) ** 2))

    return y


def GPHonewell(x, k=1, xo=0):
    return k * (x - xo) ** 2


def show_colors_default():
    axfoo = plt.subplots()[1]
    c = next(axfoo._get_lines.color_cycle)
    clist = []
    # Iterate until duplicate is found
    while c not in clist:
        clist.append(c)
    return clist

def compare_MSMs(MSMs, ref_MSM, bar = True):

    from pyemma.msm.ui.msm import EstimatedMSM
    from pyemma.msm.ui.msm import MSM as MSMobject

    assert isinstance(MSMs, list), "MSMs has to be a list"
    assert isinstance(MSMs[0], EstimatedMSM), "MSMs[0] is not an estimated-MSM-type object"
    assert isinstance(ref_MSM, MSMobject), "ref_MSM is not an MSM-type object"

    ncols = np.int(np.ceil(len(MSMs) / 2.))
    nrows = 2
    myfig, myax = plt.subplots(nrows=nrows, ncols=ncols, sharex=True, sharey=True, figsize=(10, 10))
    myylabel = '$\phi_{i%u}=\pi$' % 1
    #mytitle = '%u trajs a %u steps = %1.1e aggregated in each panel' % (nsub, tmax, ntaggregated)

    #myfig.suptitle(mytitle, horizontalalignment='center', fontsize=25, x=.6, y=1.05)


    for jj, (iax, MSM) in enumerate(zip(myax.flatten(), MSMs)):
        assert MSM.nstates == ref_MSM.nstates, "MSM and ref-MSM have different nstates, (%u != %u)"%(MSM.nstates, ref_MSM.nstates)

        # Code re-use!
        show_LEVS_from_T(ref_MSM.transition_matrix, MSM.transition_matrix,
                         cpi = MSM.count_matrix_active.sum(1),
                         axes_list=iax,
                         bar = bar)

        # Delete some of decorations of compare_LEVS...
        iax.set_ylabel('')
        iax.set_xlabel('')
        iax.legend([] * 3, [] * 3)

        iax.set_title('$r_0=%u$' % jj, fontsize=25, verticalalignment='bottom')
        iax.text(MSM.nstates - 1.5, .65, '$t_{%u}=%3.1f$' % (1, -MSM.lagtime / np.log(MSM.eigenvalues()[1])),
                 fontsize=25, color='blue', backgroundcolor='white')

        if ref_MSM is not None:
            iax.text(ref_MSM.nstates - 1.5, .75, '$t_{%u}=%3.1f$' % (1, ref_MSM.timescales()[0]), fontsize=25, color='green',
             backgroundcolor='white')

        # Cosmetics
        if jj >= len(MSMs)-ncols:
            iax.set_xlabel('state index i', fontsize=25);
            iax.set_xticks(np.arange(0, ref_MSM.nstates))
            iax.set_xticklabels(iax.get_xticks(), fontsize=25)
            iax.set_xlim((-.5, ref_MSM.nstates))
        if not jj % nrows:
            iax.set_yticklabels(iax.get_yticks(), fontsize=25)
            iax.set_ylabel(myylabel, fontsize=40);

        iax.set_ylim([0, 1])

# More cosmetics
    myax[0, 0].legend(loc=2)
    myfig.tight_layout()

def show_LEVS_from_T(TT,
                     ET = None,
                     connected_set = None,
                     cpi = None,
                     figsize=(10, 10),
                     pad=1,
                     statemax=1,
                     info_text=False,
                     axes_list=None,
                     bar=False,
                     potential = None,
                     equilibrated_pi = None,
                     x_axis = None,
                     fake_renorm = False):
    # TT=True T
    # ET=Estimated T
    # cpi=pi estimated by counts



    # Compute the true quantities
    lambdas = np.abs(msm.analysis.eigenvalues(TT))
    its = msm.analysis.timescales(TT)

    LEVS = msm.analysis.eigenvectors(TT, right = False)
    # Make sure the argmax(np.abs(LEV)) always has postive value (for visual comparison)
    sign_points = np.argmax(np.abs(LEVS),0)
    sign_points = LEVS[[sign_points],[np.arange(len(LEVS))]]
    LEVS = LEVS / np.abs(LEVS).sum(0)[np.newaxis, :]
    LEVS = LEVS * np.sign(sign_points)
    Tnstates = LEVS.shape[0]

    # Prepare for connectivity issues
    if connected_set is None:
        cset = np.arange(Tnstates)
    else:
        cset = connected_set

    # Compute the estimated quantities
    if ET is not None:
        Enstates = ET.shape[0]
        ELEVS = msm.analysis.eigenvectors(ET, right=False)
        # Make sure the argmax(np.abs(LEV)) always has postive value (for visual comparison)
        sign_points = np.argmax(np.abs(ELEVS),0)
        sign_points = ELEVS[[sign_points],[np.arange(len(ELEVS))]]
        ELEVS = ELEVS * np.sign(sign_points)
        ELEVS = ELEVS / np.abs(ELEVS).sum(0)[np.newaxis, :]
        elambdas = np.abs(msm.analysis.eigenvalues(ET))
        eits = np.abs(msm.analysis.timescales(ET))

        if Enstates != Tnstates:
            assert connected_set is not None, "The true MSM and the estimated MSM have different" \
                                              " nstates, yet you did not provide a connected set (%u, %u)"%(Tnstates, Enstates)
        if connected_set is not None:
            assert isinstance(connected_set, np.ndarray), "connected set is not an np.ndarray"
            assert connected_set.ndim is 1, "connected set should have dimension one"
            assert connected_set.shape[0] == Enstates, "The length of the connected set does not match the nstates" \
                                                       " of the estimated MSM (%u, %u)"%(connected_set.shape[0], Enstates)
        #assert LEVS.shape[0] == ELEVS.shape[0], 'The MSMs you want to compare have different nstates (%u vs %u)' %(LEVS.shape[0], ELEVS.shape[0])

    if potential is not None:
        assert isinstance(potential, np.ndarray), "Potential is not an np.ndarray"
        assert potential.ndim is 1
        assert len(potential) == Tnstates, "Potential array has less elements than nstates (%u<%u)"%(len(potential), nstates)

    # Choose the type of x axis (state indices or 1D-coordinate)
    if x_axis is None:
        myxlabel = 'state index i'
        x = np.arange(Tnstates)
    else:
        assert isinstance(x_axis, np.ndarray), "Cannot understand the x-axis you parsed me"
        assert x_axis.ndim is 1, "The x-axis should be a np.ndarray with ndim = 1, not %u"%x_axis.ndim

        myxlabel = 'x / arb. un.'
        x= x_axis
        if potential is not None:
            assert len(x_axis) == len(potential), "The coordinate and the potential have different lenghts"

    # Create figure
    if axes_list is None:
        myfig, myaxes = plt.subplots(ncols=1, nrows=statemax, sharex=True, figsize=figsize);
    else:
        myaxes = axes_list

    # Labels for y axis
    myylabel = [('$\phi_{i%u} = \pi$' % 0)]
    for jj in range(1,statemax):
        myylabel.append('$\phi_{i%u}$'%jj)

    # Not very clean, will fix later
    try:
        myaxes.__iter__()
    except:
        myaxes = [myaxes]

    # False renormalization for the true EVs, in case visual comparison is not good
    fac = 1
    if fake_renorm:
        fac = Tnstates*1./Enstates

    ### PLOT
    for ii, iax in enumerate(myaxes):

        #### PLOT
        ## Choose the type of
        if bar:
            # True vectors
            iax.bar(x[:], fac*LEVS[:, ii], align='center', width=.1, color='g', label='true');
            # Estimated vectors
            if ET is not None:
                iax.bar(x[connected_set].squeeze() - .1, ELEVS[:, ii], align='center', width=.1, color='b',
                               label='estimated');
            # Counts
            if ii == 0 and cpi is not None:
                iax.bar(x[connected_set].squeeze() + .1, cpi / cpi.sum(),
                        align='center', width=.1, color='r', label='counts'
                        )

        else:
            # True vectors
            iax.plot(x[:], fac*LEVS[:, ii], color='g', label='true');
            # Estimated vectors
            if ET is not None:
                iax.plot(x[connected_set], ELEVS[:, ii], color='b', label='estimated');
            # Counts
            if ii == 0 and cpi is not None:
                    iax.plot(x[connected_set], cpi / cpi.sum(),
                             color='r', label='counts'
                            )

        # Y- Axis shaping and labelling
        iax.set_ylim([-np.max(abs(LEVS[:, ii]*fac) * 1.5),
                      +np.max(abs(LEVS[:, ii]*fac) * 1.5)])

        iax.set_ylabel(myylabel[ii], fontsize = 40)

        # X axis labelling
        iax.set_xlim((x[:].min()-np.diff(x)[0]*.5, x[:].max() + np.diff(x)[0]*.5))

        # Black baseline at zero
        iax.plot(iax.get_xlim(), np.array([0,0]), color='k')

        # More specialities of the first axis
        if ii == 0:
            iax.set_ylim([0, iax.get_ylim()[1]])
            if potential is not None:
                potential -= potential.min()
                potential /= potential.max()
                potential *= iax.get_ylim()[1]*.8

                iax.fill_between(x, np.zeros_like(potential), potential,
                                 color = 'gray', alpha = .75, zorder = 10)
            # Dress up
            if equilibrated_pi is not None:
                assert isinstance(equilibrated_pi, np.ndarray)
                assert equilibrated_pi.shape[1] == 2
                iax.plot(equilibrated_pi[:,0], equilibrated_pi[:,1], '--k', label = 'eq counts')

        # Show the lambdas and its
        if info_text:
            iax.text(iax.get_xlim()[1] +pad/10., + np.mean(iax.get_ylim()), 'True:\n\n'
                                                                            '$\lambda_{%u}=%14.7f$\n\n'
                                                                            '$t_{%u}=%10.5f$'% (ii, lambdas[ii], ii, its[ii]),
                     fontsize=25, color='green', multialignment='left', va='center')

            if ET is not None:
                iax.text(iax.get_xlim()[1] + pad, + np.mean(iax.get_ylim()), 'MLE-estimation:\n\n'
                                                                                 '$\lambda_{%u}=%10.5f$\n\n'
                                                                                 '$t_{%u}=%10.5f$' % (
                                ii, elambdas[ii], ii, eits[ii]),
                                fontsize=25, color='blue', multialignment='left', va='center')

        iax.legend(loc=0, fontsize='15', ncol=2)
        iax.set_yticklabels(iax.get_yticks(), fontsize=15)




    # X-axis shaping and labelling
    iax.set_xlabel(myxlabel, fontsize = 20)
    if x_axis is None:
        iax.set_xticks(x)
        iax.set_xticklabels(iax.get_xticks(), fontsize=25)

    if axes_list is None:
        myfig.tight_layout()


def plot_MC_traj(rt, inset=1e3, microstates=None, figsize=(10, 5)):
    myfig, myaxes = plt.subplots(nrows=1, ncols=2, sharey=True, figsize=figsize);
    inset = int(inset)
    if microstates == None:
        rt = microstates2indeces(rt)
        microstates = np.unique(rt)

    myaxes[0].plot(rt[:inset])
    nstates = len(np.unique(rt))
    myaxes[0].set_ylim([-1, nstates]);
    myaxes[0].set_ylim([-1, nstates]);
    myaxes[0].set_ylabel('state index', fontsize=25);
    myaxes[0].set_yticks(np.arange(0, nstates))
    myaxes[0].set_yticklabels(myaxes[0].get_yticks(), fontsize=20)
    #myaxes[0].set_ytic  label('state index', fontsize=40);
    myaxes[0].set_xlabel('t / frames', fontsize='xx-large');
    myaxes[1].plot(rt);
    myaxes[1].set_xlabel('t / frames', fontsize='xx-large');
    myfig.tight_layout()


def propagate_T_show(TT, nt, tau=0):
    # Allocate
    pt = np.zeros((len(TT), nt));

    myfig, myaxes = plt.subplots(figsize=(10, 5))

    # Initialize
    pt[:, 0] = np.random.rand(len(TT))
    pt[:, 0] = pt[:, 0] / np.sum(pt[:, 0])

    # Propagate
    for t in np.arange(1, nt):
        pt[:, t] = np.dot(np.transpose(pt[:, t - 1]), TT)

    # Plot
    myaxes.plot(pt[:, :].T);
    myaxes.set_ylim(0, 1);
    myaxes.set_xlabel('time / frames', fontsize=25)
    myaxes.set_ylabel('p$_i$ ', fontsize=25)

    # Decorate
    pi = get_pi_from_T(TT)
    for ll in np.arange(0, len(TT)):
        label = '$\pi_%u$' % ll
        myaxes.text(nt + .01 * nt, pi[ll], label, fontsize=25)

    myaxes.text(nt + .1 * nt, .75, '$\mathbf{p}(t+\\tau)^\\dagger=\mathbf{p}(t)^\\dagger\mathbf{T}_\\tau$', fontsize=25)

    # Plot a timme laged if wanted
    if np.sum(tau) > 0:
        TTtau = np.linalg.matrix_power(TT, tau)
        ptau = np.zeros((len(TT), nt / tau))
        ptau[:, 0] = pt[:, 0]
        for t in np.arange(1, nt / tau):
            ptau[:, t] = np.dot(np.transpose(ptau[:, t - 1]), TTtau)

        myaxes.plot(np.arange(0, nt, tau), ptau.T, ' o');
        myaxes.text(nt + .1 * nt, .25, '$\mathbf{p}(t+k\\tau^\\dagger)=\mathbf{p}(t)\mathbf{T}^k_\\tau$', fontsize=25)
    myfig.tight_layout


def get_pi_from_counts(r, microstates=None, normed='True'):
    if microstates == None:
        r = microstates2indeces(r)
        cpi, e = np.histogram(r, bins=len(np.unique(r)))
    else:
        edges = np.append(microstates - .5, microstates[-1] + .5)
        cpi, e = np.histogram(r, bins=edges)
    if normed:
        cpi = cpi * 1.0 / len(r)
    return cpi


def compare_long_vs_distributed(TT, ntaggregated, nsub, n_rev_iter=10, uprior=0.0001, compare_algos=False,
                                verbose=False, equilibrate=False):
    nsub = int(nsub)
    tmax = np.round(ntaggregated * 1. / nsub).astype('int')

    ncols = np.int(np.ceil(len(TT) / 2.))
    myfig, myax = plt.subplots(nrows=2, ncols=ncols, sharex=True, sharey=True, figsize=(10, 10))

    tpi = msm.analysis.stationary_distribution(TT)
    its = msm.analysis.timescales(TT)[1:]

    myylabel = '$\phi_{i%u}=\pi$' % 1
    mytitle = '%u trajs a %u steps = %1.1e aggregated in each panel' % (nsub, tmax, ntaggregated)

    myfig.suptitle(mytitle, horizontalalignment='center', fontsize=25, x=.6, y=1.05)

    if equilibrate:
        neq = np.round(tmax / 100. * equilibrate)

    MSMs = []
    for jj, iax in enumerate(myax.flatten()):

        # Call py-EMMA for a good time
        trajs = (msm.generation.api.MarkovChainSampler(TT)).trajectories(nsub, tmax, start=jj)
        MSMs.append(msm.estimate_markov_model(trajs, 1))

        # Code re-use!
        show_LEVS_from_T(TT, MSMs[-1].transition_matrix,
                         cpi=MSMs[-1].count_matrix_active.sum(1),
                         axes_list=iax)

        # Delete some of decorations of compare_LEVS...
        iax.set_ylabel('')
        iax.set_xlabel('')
        iax.legend([] * 3, [] * 3)

        #t,piconv[j,:,:]  =countmatrix2transitionmatrix_rev_est_JCP2011 (c, niter=n_rev_iter, conv=True);
        #__,piconv2[j,:,:]=kyle_new(c, niter=n_rev_iter)# niter=n_rev_iter, conv=True, verbose=verbose)
        #       # __,piconv2[j,:,:]=countmatrix2transitionmatrix_rev_est_kyleJCTC(c, niter=n_rev_iter, conv=True)
        #pi=get_pi_from_T(t)

        if equilibrate:
            iax.bar(np.arange(0, len(TT)) + .2, epi, align='center', width=.1, color='c', label='counted, eq');

        iax.set_title('$r_0=%u$' % jj, fontsize=25, verticalalignment='bottom')
        iax.text(len(TT) - 1.5, .65, '$t_{%u}=%3.1f$' % (1, -MSMs[-1].lagtime / np.log(MSMs[-1].eigenvalues()[1])),
                 fontsize=25, color='blue', backgroundcolor='white')
        iax.text(len(TT) - 1.5, .75, '$t_{%u}=%3.1f$' % (1, its[0]), fontsize=25, color='green',
                 backgroundcolor='white')
        if jj >= len(TT) / 2:
            iax.set_xlabel('state index i', fontsize=25);
            iax.set_xticks(np.arange(0, len(TT)))
            iax.set_xticklabels(iax.get_xticks(), fontsize=25)
            iax.set_xlim((-.5, len(TT)))
        if not jj % 2:
            iax.set_yticklabels(iax.get_yticks(), fontsize=25)
            iax.set_ylabel(myylabel, fontsize=40);

        iax.set_ylim([0, 1])

    # More cosmetics
    myax[0, 0].legend(loc=2)
    myfig.tight_layout()

    #if not compare_algos:
    #    return piconv
    #else:
    #    return piconv,piconv2

    return MSMs


def construct_T_with_V(V, x):
    T = np.zeros((len(x), len(x)))
    for ii in np.arange(0, len(x)):
        # Jump to next
        if ii != len(x) - 1:
            jj = ii + 1
            T[ii, jj] = np.min((1, np.exp(-(V[jj] - V[ii]))))
        if ii != 0:
            # Jump to prev
            jj = ii - 1
            T[ii, jj] = np.min((1, np.exp(-(V[jj] - V[ii]))))
        # Stay here
        T[ii, ii] = 1
        # Normalize
        T[ii, :] = T[ii, :] / np.sum(T[ii, :])

    return T

def show_ITS_from_T(T, tau=1, inset=5):
    its = get_ITS_from_T(T, tau=tau)
    myfig, myaxes = plt.subplots(nrows=1, ncols=2, sharey='row', figsize=(15, 5))
    myaxes[0].plot(its[:5], '-o')
    myaxes[0].set_ylabel('$t_i$', fontsize=25)
    myaxes[1].plot(its, '-o')
    myaxes[0].set_yticklabels((myaxes[0].get_yticks()), fontsize=15)
    myaxes[0].set_xlabel('$i$', fontsize=25)
    myaxes[1].set_xlabel('$i$', fontsize=25)


def show_pi_boltzman(pi, x, V, epi=[0], modelname='model'):
    myfig, myaxes = plt.subplots(ncols=1, nrows=1, figsize=(15, 10))
    myaxes.set_xlabel('$x / a.u. $', fontsize=40)
    myaxes.set_ylabel('$\\phi_0 = \\pi / a.u. $', fontsize=40)
    myaxes.plot(x, pi, label='$\phi(x)$')
    myaxes.plot(x, np.exp(-V) / np.sum(np.exp(-V)), label='$\\frac{1}{Z}e^{-\\beta V(x)}$');
    if np.sum(epi) > 0:
        if len(epi) != len(x):
            x = np.linspace(x[0], x[-1], len(epi))
        modellabel = '$\\pi\mathrm{(%s)}$' % modelname
        myaxes.plot(x, epi, label=modellabel);

    myaxes.legend(fontsize='35', loc=2)


def plot_potential_vs_x(x, V):
    myfig, myaxes = plt.subplots(ncols=1, nrows=1, figsize=(15, 10))
    myaxes.plot(x, V, label='$V(x)$');
    myaxes.set_ylabel('$V / \kappa T$', fontsize=40)
    myaxes.set_xticklabels(myaxes.get_xticks(), fontsize=25)
    myaxes.set_yticklabels(myaxes.get_yticks(), fontsize=25)
    myaxes.set_xlabel('$x / a.u. $', fontsize=40)


def plot_ensemble_evolution(x, pt):
    import matplotlib

    fig = plt.figure(figsize=(10, 10));
    nt = np.size(pt, axis=1)
    ax = fig.add_subplot(111, projection='3d');
    X, Y = np.meshgrid(np.arange(0, nt), x)
    ax.plot_surface(X, Y, pt, cmap=matplotlib.cm.gray, linewidth=0, cstride=20, rstride=1, antialiased=False,
                    alpha=1.95);
    #ax.plot_wireframe(X,Y,pt, cmap=cm.gray,linewidth=1, cstride=2, rstride=2, antialiased=True,alpha=.25);
    ax.contour(X, Y, np.tile(pt[:, 0], (nt, 1)).T, zdir='x', offset=-nt * .10, cmap=matplotlib.cm.coolwarm, vmin=0,
               vmax=1);
    ax.contour(X, Y, np.tile(pt[:, -1], (nt, 1)).T, zdir='x', offset=nt + 1, cmap=matplotlib.cm.coolwarm, vmin=0,
               vmax=1);
    ax.set_xlabel('t / frames')
    ax.set_ylabel('x / arb. u')
    ax.view_init(50, -50)
    fig.tight_layout()


def plot_MC_traj_coord(rt, x, figsize=(10, 5), dt=1):
    myfig, myaxes = plt.subplots(nrows=1, ncols=1, figsize=figsize);
    myaxes.plot(np.arange(0, len(rt)) * dt, x[rt])
    myaxes.set_ylabel('x(t) / arb. u.', fontsize=25);
    myaxes.set_yticklabels(myaxes.get_yticks(), fontsize=20)
    myaxes.set_xlabel('t / frames', fontsize='xx-large');
    myaxes.set_xticklabels(myaxes.get_xticks(), fontsize=20)
    myfig.tight_layout()


def plot_stuff(rt, inset=[], histo=[], figsize=(10, 5), dt=1, ylabel='r(t) / arb u', xlabel=' t / arb u', bins=[],
               zero=False, return_fig=False):
    nbins = 10
    idxtraj = (0, 0)
    idxhist = (0, 1)
    nplots = 1

    if histo:
        nplots += 1
        idxhist = (0, 1)

    if inset:
        inset = int(inset * dt)
        nplots += 1
        idxtraj = (0, 1)
        idxhist = (0, 2)

    # This plot I know for sure I'll have always
    myfig, myaxes = plt.subplots(nrows=1, ncols=nplots, sharey=True, figsize=figsize, squeeze=False);
    myaxes[idxtraj].plot(np.arange(0, len(rt)) * dt, rt)
    myaxes[idxtraj].set_xlabel(xlabel, fontsize=15);
    myaxes[idxtraj].set_xticklabels(myaxes[idxtraj].get_xticks(), fontsize=10)
    myaxes[idxtraj].set_ylim([np.min(rt) - np.std(rt) / 2., np.max(rt) + np.std(rt) / 2.])
    myaxes[idxtraj].set_xlim([0, len(rt) * dt])
    if zero:
        myaxes[idxtraj].plot(np.arange(0, len(rt)) * dt, np.zeros_like(rt), '--k')

    if inset:
        myaxes[0, 0].plot(np.arange(0, len(rt)) * dt, rt)
        myaxes[0, 0].set_xlim([0, inset])
        myaxes[0, 0].set_xticklabels(myaxes[0, 0].get_xticks(), fontsize=10)
        myaxes[0, 0].set_xlabel(xlabel, fontsize=15);

    if histo:

        if bins:
            nbins = bins

        myaxes[idxhist].hist(rt, orientation='horizontal', bins=nbins)
        myaxes[idxhist].set_xlabel(' N ', fontsize=15);

    myaxes[0, 0].set_ylabel(ylabel, fontsize=15);
    myaxes[0, 0].set_yticklabels(myaxes[idxtraj].get_yticks(), fontsize=15)
    myfig.tight_layout()

    if return_fig:
        return myaxes[idxtraj]


def get_lagtimes_from_r(r, lagtimes, neig=10, dt=1):
    its = np.zeros((len(lagtimes), neig))
    for ii in np.arange(0, len(lagtimes)):
        c = get_countmatrix(r, lag=lagtimes[ii])
        t = countmatrix2transitionmatrix_rev_est_JCP2011(c)
        lits = get_ITS_from_T(t, tau=lagtimes[ii])
        its[ii, :] = lits[:neig] * dt
        print(lagtimes[ii] * dt, its[ii, :])

    return its


def get_ITS_from_rt(rt, lagtimes, neig=10, dt=1):
    its = np.zeros((len(lagtimes), neig))
    for ii in np.arange(0, len(lagtimes)):
        c = get_countmatrix(rt, lag=lagtimes[ii])
        t = countmatrix2transitionmatrix_rev_est_JCP2011(c)
        lits = get_ITS_from_T(t, tau=lagtimes[ii])
        its[ii, :] = lits[:neig] * dt
        print(lagtimes[ii] * dt, its[ii, :])

    return its


def plot_ITS(its, lagtimes, figsize=(10, 5), dt=1):
    myfig, myaxes = plt.subplots(nrows=1, ncols=1, figsize=figsize);
    myaxes.plot(lagtimes * dt, its * dt, '-o')
    myaxes.plot(lagtimes * dt, lagtimes * dt, '-', color='k')
    myaxes.set_ylabel('$t_i(\\tau) / frames$', fontsize=25);
    myaxes.set_yticklabels(myaxes.get_yticks(), fontsize=20)
    myaxes.set_xlabel('$\\tau / frames$', fontsize='xx-large');
    myaxes.set_xticklabels(myaxes.get_xticks(), fontsize=20)
    myfig.tight_layout()
    return myfig


def boltzmann(V):
    b = np.exp(-V) / np.sum(np.exp(-V))
    return b


def show_QT(T, x, xlabel='', ylabel='', title=''):
    myfig, myaxes = plt.subplots(ncols=1, nrows=1, figsize=(10, 10))
    myaxes.set_ylabel(xlabel, fontsize=40)
    myaxes.set_xlabel(ylabel, fontsize=40)
    myaxes.pcolormesh(x, x, T);
    myaxes.invert_yaxis()
    myaxes.set_title(title, fontsize=40)


class metrics:
    import mdtraj as md
    import itertools

    def sample_random_distances(self, trajfile, topfile, ndist=10, tmin=0, nsamples=100, path='./', descriptor='sample',
                                maxind=1000):
        t = metrics.md.load(trajfile, top=topfile)
        pairs = list(metrics.itertools.combinations(list(range(np.min((maxind, t.n_atoms)))), 2))
        pairs = np.array(pairs)
        fname = trajfile[:-4]
        for ii in np.arange(0, nsamples):
            nameinf = '%s%s.%s.%04u.info' % (path, fname, descriptor, ii)
            nameout = '%s%s.%s.%04u.dist.dat' % (path, fname, descriptor, ii)
            thislist = np.random.randint(len(pairs), size=ndist)
            d = metrics.md.compute_distances(t, pairs[thislist])
            np.savetxt(nameinf, pairs[thislist], fmt='%04u %04u')
            np.savetxt(nameout, d, fmt='%12.6f')

    def corr_from_U_and_sigma2(self, U, sigma2):
        c = np.dot(sigma2, U).T / np.tile(np.sqrt(np.diag(sigma2)), (len(sigma2), 1))

    #        return metrics.c

    def most_corr_from_U_and_sigma(self, flist,
                                   maxindicators=1,
                                   lag=10,
                                   ticaidx=0):

        input_parameters = []
        for ff in flist:
            input_parameter_list = np.genfromtxt('%s.info' % ff)
            sigma2 = np.genfromtxt('%s.covar.dat' % ff)
            U = np.genfromtxt('%s.lag.%u.V.dat' % (ff, lag))

            corr_from_U_and_sigma2(U, sigma2)
            idxs = np.argsort(abs(c[ticaidx, :]))
            idxs = idxs[::-1]
            idxs = idxs[0:maxindicators]
            toadd = np.array(input_parameter_list[idxs, :].flatten(), ndmin=2)

            if np.size(input_parameters) == 0:
                input_parameters = toadd
            else:
                input_parameters = np.append(input_parameters, toadd, axis=0)

        return input_parameters


def show_pi_convergences(piconv, true_pi=[]):
    # Get some data
    nstat = piconv.shape[-2]
    niter = piconv.shape[-1]
    print('niter,nstat', niter, nstat)
    if np.size(true_pi) != 0:
        pi = np.zeros((niter, nstat))
        pi[:, :] = true_pi[np.newaxis, :]

    if np.rank(piconv) > 2:
        nruns = piconv.shape[-3]
        ncols = np.int(np.ceil(nruns / 2.))

    else:
        nruns = 1
        ncols = 2

    print('cols, runs, rank', ncols, nruns, np.rank(piconv))
    # Set the figure
    myfig, myaxes = plt.subplots(nrows=2, ncols=ncols,
                                 figsize=(10, 10), squeeze=True,
                                 sharex=True, sharey=True)

    # Plot all convergences
    for cc in np.arange(0, nruns):
        idx = np.unravel_index(cc, (2, ncols), )
        myaxes[idx].plot(piconv[cc].T, 'b');
        myaxes[idx].set_ylim(0, 1);
        myaxes[idx].set_xlim(0, niter);
        myaxes[idx].plot(pi, '--k')

        # Decorate
        if idx[1] == 1:
            ii = 0
            for pp in true_pi:
                label = '$\pi_%u$' % ii
                ii += 1
                myaxes[idx].text(niter + .01 * niter, pp, label, fontsize=25)

                #myaxes.set_ylim(0,1);
        if idx[0] == 1:
            myaxes[idx].set_xlabel('iteration ', fontsize=25)

        if idx[1] == 0:
            myaxes[idx].set_ylabel('$\\pi_i$ ', fontsize=25)

    myfig.tight_layout()


def countmatrix2transitionmatrix_rev_est_kyleJCTC(c, niter=10, verbose=False, conv=False):
    n = len(c)
    piconv = np.zeros((n, niter))

    if verbose:
        print(c.sum(1) / c.sum())

    # Initialize
    Na = c.sum(1)

    # Define a symmetric count matrix for the first time
    X = (c + c.T) / 2.

    # Start the iteration
    for kk in np.arange(0, niter):

        # Compute the row sums of X
        Xrs = X.sum(1)

        # Update the diagonal
        XNa = Xrs / Na
        np.fill_diagonal(X, c.diagonal() * XNa)

        # Update the off-diagonal
        for a in np.arange(0, n):
            for b in np.arange(a + 1, n):
                # Denominator
                denom = (Na[a] / Xrs[a] + Na[b] / Xrs[b])

                X[a, b] = (c[a, b] + c[b, a]) / denom

                X[b, a] = X[a, b]


        # Construct the respective T
        T = X / X.sum(1)

        # Store the respective pi to look at the convegence
        #       piconv[:,kk]=get_pi_from_T(T.T)
        piconv[:, kk] = X.sum(1) / X.sum()
        if verbose:
            print(T)
            print(' ')

    if conv:
        return T, piconv
    else:
        return T


def kyle_new(C, niter=25):
    CS = C + C.T
    Caa = C.diagonal()
    N_i = C.sum(1)

    X = C + C.T
    pi_trace = []
    for k in range(niter):
        pi_trace.append(X.sum(0) / X.sum())
        X_s = X.sum(1)
        new_diag = Caa * X_s / N_i
        Q = N_i / X_s
        X = CS / (Q[:, np.newaxis] + Q[np.newaxis])
        np.fill_diagonal(X, new_diag)

    T = X / X.sum(1)
    return T.T, np.array(pi_trace).T


def plot_2Dcommitor(A, B, q_AB, centers, cset=[], PDF=[], x_edges=[], y_edges=[],
                    label_commitor=False, nTS=1, xlabel='x', ylabel='y', title='commitor',
                    alpha=.5):
    # Plot the commitor between the argmaxs of the first LEVs
    plt.figure(figsize=(15, 15))
    ncolors = 100
    colors = plt.get_cmap('jet')(np.linspace(0, 1.0, ncolors))
    if cset == []:
        cset = np.arange(len(centers))

    for ii, cc in enumerate(centers[cset]):

        # Plot the centers of the connected set
        q = q_AB[ii]
        plt.plot(cc[0], cc[1], 'o', markersize=25, color=colors[np.argmin(np.abs(q - np.linspace(0, 1.0, ncolors)))],
                 alpha=alpha)

        if label_commitor:
            # Plot the commitor
            plt.text(cc[0], cc[1] + .5e-2, '%3.2f' % q,
                     size=15, color='k', weight='bold', va='center', ha='center', );


        # For the TPT-sets
        if np.in1d(ii, A):
            # Add the index
            plt.text(cc[0], cc[1], 'A',
                     size=15, color='w', weight='bold', va='center', ha='center', );
            # Add the index
            plt.text(cc[0], cc[1] - .5e-2, '%u' % cset[ii],
                     size=15, color='k', weight='bold', va='center', ha='center', );

        elif np.in1d(ii, B):
            plt.text(cc[0], cc[1], 'B',
                     size=15, color='w', weight='bold', va='center', ha='center', );
            # Add the index
            plt.text(cc[0], cc[1] - .5e-2, '%u' % cset[ii],
                     size=15, color='k', weight='bold', va='center', ha='center', );

    # Addd the pdf as a backdrop
    if PDF != []:
        X, Y = np.meshgrid(x_edges, y_edges)
        plt.contour(X, Y, PDF.T, 25, colors='k')

    plt.title(title)
    plt.gca().set_aspect('equal', adjustable='box-forced')
    plt.tight_layout()
    plt.xlabel(xlabel, fontsize=20)
    plt.ylabel(ylabel, fontsize=20)

    # Plot the disconnected centers
    plt.plot(centers[np.setdiff1d(np.arange(len(centers)), cset)][:, 0],
             centers[np.setdiff1d(np.arange(len(centers)), cset)][:, 1], 'ok'),

    # Get what consider the states closest to the TS (q=.5)
    q_sort = np.argsort(np.abs(q_AB - .5))
    closest_to_TS = cset[q_sort]



    # Plot the commitor
    for ii in np.arange(nTS):
        # Plot it
        plt.text(centers[closest_to_TS[ii], 0], centers[closest_to_TS[ii], 1], 'TS',
                 size=15, color='g', weight='bold', va='center', ha='center');

        # Add the index
        plt.text(centers[closest_to_TS[ii], 0], centers[closest_to_TS[ii], 1] - .5e-2, '%u' % closest_to_TS[ii],
                 size=15, color='k', weight='bold', va='center', ha='center', );

        plt.text(centers[closest_to_TS[ii], 0], centers[closest_to_TS[ii], 1] + .5e-2, '%3.2f' % q_AB[q_sort[ii]],
                 size=15, color='k', weight='bold', va='center', ha='center', );
    return closest_to_TS[:nTS]


def aggregate_MSMs(MSMs, estimate = False ):
    """

    returns a list with the thiscrete trajectories of the MSMs-objects contained in the MSMlist.


    :rtype : list, list of discrete trajectories

    """
    from pyemma.msm.ui.msm import EstimatedMSM
    from itertools import chain
    assert isinstance(MSMs, list), "MSMs has to be a list"
    assert isinstance(MSMs[0], EstimatedMSM), "MSMs[0] is not an estimated-MSM-type object"

    dtrajs = list(chain.from_iterable([iMSM.discrete_trajectories_active for iMSM in MSMs]))

    return dtrajs


def my_monoexp_fit(y_obs, x_obs = None, Nguess = None, tauguess = None, lambdaguess = None, return_fit = False, verbose = False):
    """
    returns the parameters of a mono-exponential fit to the observed y (yobs) on the observed x

    yfit = N * exp(-xobs * lambda) = N * exp(-xobs/tau)

    Parameters:
    -----------

    yobs:   1D numpy ndarray, corresponding to the observation at xobs

    xobs:   1D numpy ndarray, increasing. If none is given, will default to np.arange(0, len(y_obs))


    Nguess: Float, initial guess for the intensity. If none is given defaults to y_obs[0]

    tauguess:       Float, initial guess for the time-constant. If none is given, defaults to x_obs at y_obs = y_obs[0]/2
                    If tauguess is given, no lambdaguess can be given simultaneously

    lambdaguess:    Float, initialguess for the decay rate, If none is given, defaults to 1/x_obs at y_obs = y_obs[0]/2
                    If lambdaguess is given, no tauguess can be given simultaneously

    return_fit:     Boolean, default = False. If True, also the y_fit will be returned

    Returns:
    ----------

    N, tau:       fitted parameters

    or
    (N, tau), y_fit fitted parameters and the fitted observation

    """

    assert isinstance(y_obs, np.ndarray)
    assert y_obs.ndim == 1
    assert not (lambdaguess is not None and tauguess is not None), "Cannot input a guess for tau AND lambda"

    if x_obs is None:
        x_obs = np.arange(len(y_obs))
    else:
        assert isinstance(x_obs, np.ndarray)
        assert x_obs.ndim == 1

    if Nguess is None:
        Nguess = y_obs[0]

    if verbose:
        print("N   guess = ", Nguess)

    if tauguess is None and lambdaguess is None:
        tauguess = x_obs[np.argmin(np.abs(y_obs-Nguess*.5))]
        if verbose:
            print("tau guess = ", tauguess)

    elif tauguess is None and lambdaguess is not None:
        tauguess = 1./lambdaguess
        if verbose:
            print("tau guess = ", tauguess, "via lambda guess = ", lambdaguess)


    # Fit
    popt, pcov = op.curve_fit(myexp, x_obs, y_obs, p0=[Nguess, tauguess])

    # Inform
    if verbose:
        print()
        print("N    fit = ", popt[0])
        print("tau  fit = ", popt[1])

    # Return
    if return_fit:
        yfit = myexp(x_obs, popt[0], popt[1])
        return (popt[0], popt[1]), yfit
    else:
        return (popt[0], popt[1])
