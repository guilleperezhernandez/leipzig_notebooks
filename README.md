# README #

Collection of notebooks to teach one day of the 
[Master-Modul Molekülmodellierung](https://biophysik.medizin.uni-leipzig.de/lehre/molekuelmodellierung/) of the University Leipzig.

The subject of the notebooks is Molecular Kinetics and Adaptive Sampling in the context of Molecular Dynamics.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact